// 一些配置
module.exports = {
  // 是否测试项目所有文件，而不仅仅是测试套件所接触到的文件
  // 如果开启了，那么即使在所有测试用例中都没用到的模块也会列出来，当然，肯定是0的覆盖率了。
  all: false,
  // 显示覆盖率结果，html表明在项目下生成一个漂亮的html报表，text表示在终端中显示覆盖率结果

  reporter: ["html", "text"],
  // nyc除了js之外还应该处理的扩展名的列表，比如.ts之类的
  extension: [],
  // 这是通常对待处理文件进行编译的配置，比如es6语法就要进行babel编译
  require: [],
  // 这个就是代码映射，默认就是开启的
  sourceMap: true,
  // 一个需要排除特定文件的列表
  exclude: [],
  // 一个只需要包含特定文件的列表
  include: []
};
