const webpack = require("webpack");
const PATH = require("path");
const FilterConsoleLog = require("./src/utils/js/filterConsoleLog");

const isBuildDev = process.env.VUE_APP_API === "development";

const isOnline = process.env.NODE_ENV === "production";

// 打包文件配置该参数
const CDNURL = process.env.VUE_APP_CDNURL ? process.env.VUE_APP_CDNURL : "";

// 配置打包正式环境时将js与css的路径换成绝对路径
const publicPath = CDNURL ? CDNURL : "/";

// 开发中打印console.log  线上不打印console.log
let plugins = isBuildDev ? [] : [new FilterConsoleLog()];
plugins.push(
  new webpack.ProvidePlugin({
    $: "jquery",
    jQuery: "jquery",
    "windows.jQuery": "jquery"
  })
);

module.exports = {
  lintOnSave: true, // 打开代码检查
  publicPath: publicPath,
  integrity: !isBuildDev, // 部署CDN上时启用
  // outputDir: "./assets",
  // assetsDir: "./assets",
  css: { extract: isOnline }, // 打包文件时开启，无论测试还是正式
  configureWebpack: {
    resolve: {
      alias: {
        "@components": PATH.resolve(__dirname, "./src/components"),
        "@basketballCharts": PATH.resolve(
          __dirname,
          "./src/components/chartLibrary/basketball/"
        ),
        "@static": PATH.resolve(__dirname, "./src/static"),
        "@images": PATH.resolve(__dirname, "./src/static/images"),
        "@png": PATH.resolve(__dirname, "./src/static/images/png"),
        "@svg": PATH.resolve(__dirname, "./src/static/images/svg"),
        "@view": PATH.resolve(__dirname, "./src/views"),
        "@style": PATH.resolve(__dirname, "./src/static/style"),
        "@utils": PATH.resolve(__dirname, "./src/utils"),
        "@globalExtends": PATH.resolve(__dirname, "./src/globalExtends"),
        "@baseOption": PATH.resolve(__dirname, "./src/baseOption"),
        "@api": PATH.resolve(__dirname, "./src/api")
      }
    },
    module: {
      rules: [
        {
          test: /\.css$/,
          include: [
            "/src/",
            "/node_modules/element-ui/lib/" //增加此项
          ],
          loader: "style-loader!css-loader"
        },
        {
          test: /\.(js|jsx)$/,
          loader: "eslint-loader",
          exclude: [
            PATH.join(__dirname, "./src/static/fonts/iconfont.js"),
            PATH.join(__dirname, "./node_modules")
          ],
          enforce: "pre" //预先处理
        }
      ]
    },
    plugins: plugins
  },
  devServer: {
    proxy: {
      "/": {
        target: "http://stag-mxtsports.mixtmt.com",
        // ws: true,
        changeOrigin: true
      }
    }
  }
};
