module.exports = {
    root: true,
    env: {
      node: true,
      jquery:true
    },
    extends: ["plugin:vue/essential", "@vue/prettier"],
    // extends: ["plugin:vue/strongly-recommened", "@vue/prettier"],
    // 开启或者关闭看值的状态，或者去文档（README文档中有地址）查阅其含义，一定要确定没有问题再进行修改
    // 0或’off’：关闭规则。 
    // 1或’warn’：打开规则，并且作为一个警告（并不会导致检查不通过）。 
    // 2或’error’：打开规则，并且作为一个错误 (退出码为1，检查不通过)。
    rules: {
      "no-console": process.env.NODE_ENV === "production" ? "warn" : "off", // 打包根据环境是否开启log检查
      "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off", // 打包根据环境是否开启debuger检查
      'array-callback-return': 'off', // 关闭数组函数没有return的告警
      'no-script-url': 'off', // 关闭a标签href无url的告警
      'jsx-a11y/anchor-is-valid': 'off', // 关闭jsx a标签无效的告警
      'jsx-a11y/anchor-has-content': 'off', // 关闭jsx a标签无内容的告警
      'no-unused-vars': 'off', // 禁止出现未使用变量
      // "no-mixed-spaces-and-tabs": ["error", "smart-tabs"], // 是否禁止tab与空格的混合缩进
      "no-tabs": ['error', { allowIndentationTabs: true }], // 是否禁用tab
      "no-undef": 'error', // 不允许未声明的变量
      "no-unused-vars": [2, {"vars": "all", "args": "after-used"}], //不允许有声明后未使用的变量或者参数
      "no-unused-expressions": 2, //不允许无用的表达式
    },
    parserOptions: {
      parser: "babel-eslint"
    },
    overrides: [
      {
        files: ["**/__tests__/*.{j,t}s?(x)", "**/tests/unit/**/*.spec.{j,t}s?(x)"],
        env: {
          mocha: true
        }
      }
    ],
    plugins: ["vue"]
  };
  