const path = require("path");
const Mocha = require("mocha");

try {
  process.chdir(path.join(__dirname, "../../dist"));
} catch (err) {
  throw new Error(`err: ${err}; 没有找到dist目录`);
}

const mocha = new Mocha({
  timeout: "10000ms"
});

mocha.addFile(path.join(__dirname, "html-test.js"));
mocha.addFile(path.join(__dirname, "css-js-test.js"));
mocha.run();
