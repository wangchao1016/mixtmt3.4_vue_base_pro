/* eslint-disable no-undef */
const glob = require("glob-all");

describe("Checking css js files", () => {
  it("should css files", done => {
    const files = glob.sync([
      "./css/app.*.css",
      "./css/home.*.css",
      "./css/statusPage.*.css",
      "./css/basketballAnalysis.*.css",
      "./css/basketballIndex.*.css",
      "./css/basketballPlayer.*.css",
      "./css/basketballTeam.*.css",
      "./css/basketballPlayer~basketballTeam.*.css",
      "./css/basketballPlayerDetail.*.css",
      "./css/basketballTeamDetail.*.css",
      "./css/basketballPlayerDetail~basketballTeamDetail.*.css",
      "./css/chunk-vendors.*.css",
      "./css/elementUI_custom.css",
      "./css/elementUI_darkSkin.css",
      "./css/elementUI_lightSkin.css",
      "./css/reset.css"
    ]);
    if (files.length > 0) {
      done();
    } else {
      throw new Error("Not css files found");
    }
  });

  it("should js files", done => {
    const files = glob.sync([
      "./js/app.*.js",
      "./js/home.*.js",
      "./js/statusPage.*.js",
      "./js/basketballAnalysis.*.js",
      "./js/basketballIndex.*.js",
      "./js/basketballPlayer.*.js",
      "./js/basketballTeam.*.js",
      "./js/basketballPlayer~basketballTeam.*.js",
      "./js/basketballPlayerDetail.*.js",
      "./js/basketballTeamDetail.*.js",
      "./js/basketballPlayerDetail~basketballTeamDetail.*.js",
      "./js/chunk-vendors.*.js"
    ]);
    if (files.length > 0) {
      done();
    } else {
      throw new Error("Not js files found");
    }
  });
});
