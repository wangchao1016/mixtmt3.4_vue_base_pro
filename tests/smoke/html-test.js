/* eslint-disable no-undef */
const glob = require("glob-all");

describe("Checking html files", () => {
  it("should html files", done => {
    const files = glob.sync(["index.html"]);
    if (files.length > 0) {
      done();
    } else {
      throw new Error("Not html files found");
    }
  });
});
