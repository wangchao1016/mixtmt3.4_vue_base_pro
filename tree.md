----mxtsports-explorer\
  |----tree.py
  |----.env.buildDev
  |----tests\
  |  |----unit\
  |  |  |----example.spec.js
  |  |  |----.eslintrc.js
  |----.env.build
  |----README.md
  |----public\
  |  |----favicon.ico
  |  |----index.html
  |  |----css\
  |  |  |----elementUI_custom.css
  |  |  |----elementUI_lightSkin.css
  |  |  |----elementUI_darkSkin.css
  |----babel.config.js
  |----.gitignore
  |----package-lock.json
  |----package.json
  |----vue.config.js
  |----.eslintrc.js
  |----tree.md
  |----.eslintignore
  |----.vscode\
  |  |----settings.json
  |----.browserslistrc
  |----postcss.config.js
  |----.env.development
  |----src\
  |  |----App.vue
  |  |----skin\
  |  |  |----index.js
  |  |  |----components\
  |  |  |  |----index.js
  |  |  |----skinOption\
  |  |  |  |----lightSkinOption.js
  |  |  |  |----darkSkinOption.js
  |  |  |  |----index.js
  |  |----.DS_Store
  |  |----utils\
  |  |  |----js\
  |  |  |  |----filterConsoleLog.js
  |  |  |  |----passport.js
  |  |  |  |----index.js
  |  |  |  |----lib-flexible.js
  |  |  |  |----api.js
  |  |  |----thirdParty\
  |  |  |  |----ly-downloader.js
  |  |  |  |----CryptoJS\
  |  |  |  |  |----mode-ecb-min.js
  |  |  |  |  |----aes.js
  |  |  |  |----downloads.js
  |  |----main.js
  |  |----components\
  |  |  |----Footer\
  |  |  |  |----index.vue
  |  |  |----popup\
  |  |  |  |----PopupCenter.vue
  |  |  |----CheckCodeSlider\
  |  |  |  |----index.vue
  |  |  |----Loading\
  |  |  |  |----index.vue
  |  |  |----NotData\
  |  |  |  |----index.vue
  |  |  |----promptBox\
  |  |  |  |----index.vue
  |  |  |----switch\
  |  |  |  |----SliderSwitch.vue
  |  |  |----statusPage\
  |  |  |  |----404.vue
  |  |  |  |----500.vue
  |  |----static\
  |  |  |----images\
  |  |  |  |----svg\
  |  |  |  |  |----checked.svg
  |  |  |  |  |----pdf_logo.svg
  |  |  |  |  |----not_checked.svg
  |  |  |  |----png\
  |  |  |  |  |----trophy.png
  |  |  |  |  |----playbasic.png
  |  |  |  |  |----basketball_court.png
  |  |  |  |  |----expect.png
  |  |  |  |  |----404.png
  |  |  |  |  |----basketball_half_court.png
  |  |  |  |  |----login_bg.png
  |  |  |  |  |----.DS_Store
  |  |  |  |  |----creat_default.png
  |  |  |  |  |----event10.png
  |  |  |  |  |----event11.png
  |  |  |  |  |----newball1.png
  |  |  |  |  |----event12.png
  |  |  |  |  |----combinntion.png
  |  |  |  |  |----move.png
  |  |  |  |  |----event4.png
  |  |  |  |  |----match_list_logo_default.png
  |  |  |  |  |----offensive_efficiency1.png
  |  |  |  |  |----event5.png
  |  |  |  |  |----1v1-2.png
  |  |  |  |  |----event7.png
  |  |  |  |  |----offensive_efficiency2.png
  |  |  |  |  |----popup_bottom.png
  |  |  |  |  |----event6.png
  |  |  |  |  |----1v1space-2.png
  |  |  |  |  |----otherarea2.png
  |  |  |  |  |----greatarea2.png
  |  |  |  |  |----event2.png
  |  |  |  |  |----event3.png
  |  |  |  |  |----trophy1.png
  |  |  |  |  |----field.png
  |  |  |  |  |----basket_logo.png
  |  |  |  |  |----guard.png
  |  |  |  |  |----login_dec.png
  |  |  |  |  |----otherarea1.png
  |  |  |  |  |----greatarea1.png
  |  |  |  |  |----not-package.png
  |  |  |  |  |----500.png
  |  |  |  |  |----event1.png
  |  |  |  |  |----trophy2.png
  |  |  |  |  |----fieldVertical.png
  |  |  |  |  |----nodata.png
  |  |  |  |  |----popup_top.png
  |  |  |  |  |----scoring_efficiency.png
  |  |  |  |  |----1v1space.png
  |  |  |  |  |----forward.png
  |  |  |  |  |----login_ball.png
  |  |  |  |  |----webmaster_table_not_content.png
  |  |  |  |  |----loading_logo.png
  |  |  |  |  |----newBallBg_1000x670.png
  |  |  |  |  |----newball.png
  |  |  |  |  |----event8.png
  |  |  |  |  |----event9.png
  |  |  |  |  |----basketball.png
  |  |  |  |  |----defense_efficiency2.png
  |  |  |  |  |----1v1.png
  |  |  |  |  |----passing_value1.png
  |  |  |  |  |----defense_efficiency1.png
  |  |  |  |  |----login_logo_blue.svg
  |  |  |  |  |----match_roundBg.png
  |  |  |  |  |----formation.png
  |  |  |  |  |----chartView_popCloseIcon.png
  |  |  |  |  |----default_countryLogo.png
  |  |  |  |  |----passing_value2.png
  |  |  |  |  |----midfield.png
  |  |  |  |  |----outsidearea2.png
  |  |  |  |  |----default.png
  |  |  |  |  |----smallarea1.png
  |  |  |  |  |----default_person.png
  |  |  |  |  |----shoot.png
  |  |  |  |  |----outsidearea1.png
  |  |  |  |  |----score_distribution.png
  |  |  |  |  |----smallarea2.png
  |  |  |  |  |----cus_football.png
  |  |  |----style\
  |  |  |  |----css\
  |  |  |  |  |----reset.css
  |  |  |  |----scss_sass\
  |  |  |  |  |----themeVar.scss
  |  |  |  |  |----element-ui.scss
  |  |  |----iconfont\
  |  |  |  |----iconfont.js
  |  |  |  |----demo.css
  |  |  |  |----iconfont.json
  |  |  |  |----iconfont.woff
  |  |  |  |----iconfont.eot
  |  |  |  |----iconfont.ttf
  |  |  |  |----demo_index.html
  |  |  |  |----iconfont.woff2
  |  |  |  |----iconfont.svg
  |  |  |  |----iconfont.css
  |  |----views\
  |  |  |----home\
  |  |  |  |----index.vue
  |  |  |  |----.DS_Store
  |  |  |----.DS_Store
  |  |  |----exceptionalPage\
  |  |  |  |----badGateway.vue
  |  |  |  |----notFound.vue
  |  |  |----register\
  |  |  |  |----index.vue
  |  |  |----login\
  |  |  |  |----index.vue
  |  |----globalExtends.js
  |  |----store\
  |  |  |----index.js
  |  |  |----modules\
  |  |  |  |----home\
  |  |  |  |  |----mutations.js
  |  |  |  |  |----actions.js
  |  |  |  |  |----index.js
  |  |  |  |  |----state.js
  |  |----router\
  |  |  |----index.js
