# front_sportcs_v3-4 超赛体育 3.4 版本

```
  常规框架以外还支持以下功能：
  支持换肤功能
  支持pc、ipad、h5适配问题
  支持pc低版本( 版本 < ie10)浏览器提示页
  通过动态传参配置在开发、线上等不同环境下不同的静态资源路径
```

## 工程目录打印命令与所在文件位置

```
  在对项目添加内容之后务必更新一次工程目录
  脚本：tree.py
  打印命令：python3 tree.py
  打印文件位置：tree.md
```

## 开启 Eslint 代码检查

```
  为确保代码风格统一禁止完全关闭eslint， 配置之后eslintrc.js配置后添加相应功能的配置
  eslint 配置表地址 https://cn.eslint.org/docs/rules/
  需要vscode安装eslint、prettier-code formatter插件
```

## Project setup 项目设置

```
npm install
```

### Compiles and hot-reloads for development 本地环境运行

```
npm run dev
```

### Compiles and minifies for production 正式环境打包

```
npm run build
```

### Run your tests 测试环境打包

```
npm run test
```

### Lints and fixes files 代码检查

```
npm run lint
```

### Run your unit tests 测试样例

```
npm run test:unit
```

### Customize configuration 自定义配置 VUE 文档地址

See [Configuration Reference](https://cli.vuejs.org/config/).

#### 项目工程目录

```
|----.browserslistrc                                                      —— 浏览器兼容配置
|----.eslintignore                                                        —— 配置代码检查规避那些文件夹
|----.eslintrc.js                                                         —— 配置代码风格检查工具
|----.gitignore                                                           —— git 配置
|----babel.config.js                                                      —— babel 配置
|----package-lock.json                                                    —— 包记录
|----package.json                                                         —— 包管理
|----postcss.config.js                                                    —— css插件配置
|----public\                                                              —— // 根html文件与页签icon文件
|  |----favicon.ico
|  |----index.html
|----README.md                                                            —— 说明文档
|----src\                                                                 —— 工程开发
|  |----App.vue                                                           —— 根VUE文件
|  |----components\                                                       —— 组件
|  |  |----chartLibrary\                                                  —— 可视化图表
|  |  |  |----basketball\                                                    —— 比赛类型
|  |  |  |----football\
|  |  |  |  |----canvas\                                                        —— 实现方式
|  |  |  |  |----echarts\
|  |  |  |  |----html\
|  |  |----popup\                                                         —— 弹窗组件
|  |  |  |----PopupCenter.vue                                             —— 窗口在页面中心，大小可通过参数来配置
|  |----main.js                                                           —— 根 JS 文件
|  |----router\                                                           —— 路由
|  |  |----index.js
|  |----static\                                                           —— 静态资源文件
|  |  |----font\                                                          —— 字体文件
|  |  |----iconfont\                                                      —— 字体图标文件
|  |  |----images\                                                        —— 图片
|  |  |  |----png\                                                           —— 图片类型
|  |  |  |  |----popup_bottom.png
|  |  |  |  |----popup_top.png
|  |  |  |  |----webmaster_table_not_content.png
|  |  |  |----svg\
|  |  |  |  |----login_logo.svg
|  |  |  |  |----mixtmt_logo_blue.svg
|  |  |  |  |----notice_btn.svg
|  |  |  |  |----notice_btn1.svg
|  |  |----style\                                                         —— 样式表
|  |  |  |----css\
|  |  |  |  |----reset.css
|  |  |  |----scss_sass\
|  |  |  |  |----element-ui.scss
|  |  |  |  |----mixin.scss
|  |  |  |  |----theme.scss
|  |  |  |  |----themeVar.scss
|  |----store\                                                            —— 状态管理 VUEX
|  |  |----index.js                                                          —— 根文件
|  |  |----modules\                                                          —— 模块
|  |  |  |----home\
|  |  |  |  |----actions.js
|  |  |  |  |----index.js
|  |  |  |  |----mutations.js
|  |  |  |  |----state.js
|  |----utils\                                                            —— 工具 公用方法
|  |  |----js\                                                               —— 二次封装（自行封装）
|  |  |  |----api.js
|  |  |  |----index.js
|  |  |  |----passport.js
|  |  |----thirdParty\                                                       —— 第三方
|  |  |  |----CryptoJS\
|  |  |  |  |----aes.js
|  |  |  |  |----mode-ecb-min.js
|  |----views\                                                           —— 页面
|  |  |----exceptionalPage\                                                 —— 异常处理页面
|  |  |  |----badGateway.vue
|  |  |  |----notFound.vue
|  |  |----home\                                                            —— 首页
|  |  |  |----childrenView\                                                    —— 子页面
|  |  |  |  |----header\                                                          —— header
|  |  |  |  |  |----index.vue
|  |  |  |  |----homeList\                                                        —— 赛事列表
|  |  |  |  |  |----index.vue
|  |  |  |  |----scheduleList\                                                    —— 赛程页
|  |  |  |  |  |----basketball\                                                      —— 赛事
|  |  |  |  |  |  |----cup\                                                             —— 比赛类型
|  |  |  |  |  |  |  |----index.vue
|  |  |  |  |  |  |----league\
|  |  |  |  |  |  |  |----index.vue
|  |  |  |  |  |----football\
|  |  |  |  |  |  |----cup\
|  |  |  |  |  |  |  |----index.vue
|  |  |  |  |  |  |----league\
|  |  |  |  |  |  |  |----index.vue
|  |  |  |----index.vue
|  |  |----login\                                                           —— 登陆页面
|  |  |  |----index.vue
|  |  |----webmaster\                                                       —— 站点管理页 （独立处理样式与逻辑、最好与其他页面降低耦合）
|  |  |  |----childrenView\
|  |  |  |  |----eventInforMation\
|  |  |  |  |  |----index.vue
|  |  |  |  |----groupMagmet\
|  |  |  |  |  |----index.vue
|  |  |  |  |----logInforMation\
|  |  |  |  |  |----index.vue
|  |  |  |  |----userMagmet\
|  |  |  |  |  |----index.vue
|  |  |  |  |----webMasterFooter.vue
|  |  |  |  |----webMasterHeader.vue
|  |  |  |  |----webMasterPopup.vue
|  |  |  |  |----webMasterSwitch.vue
|  |  |  |----index.vue
|----tests\                                                                —— 测试工具
|  |----unit\
|  |  |----.eslintrc.js
|  |  |----example.spec.js
|----tree.md                                                               —— 工程树文件
|----tree.py                                                               —— 生成工程树工具
|----vue.config.js                                                         —— vue-cli脚手架配置文件
```

#### 项目规范

###### 书写规范

1. 命名以百度翻译为准。
2. 常量的命名规则 NODE_MODULES //详细注解
3. 变量命名规则 nodeModules
4. VUEX store 命名规范 nodeModules
5. VUEX mutations 命名规范 mutation_nodeModules
6. VUEX actions 命名规范 action_nodeModules
7. VUEX 调用方式 ...mapSate ...mapMutations ...mapActions
8. 全局变量存放位置 src/store/index.js
9. 全局引入外部 js 方法命名规范 root*elementUI (root*开头)
10. 组件命名规则 NodeModules
    每个组件中 name 属性都要写组件名 NodeModules
    如果一个组件文件下只有有个组件例如 Footer 这样的，里面只需要写 index.vue
11. template 模板书写规范 逻辑嵌套不要过于复杂，方便可读性
    独立组件或者页面顶层命名做区分，最好为 ID 作为唯一名称。（如果命名重复添加当前版本号）
12. SCSS 书写默认风格全局文件 src/static/style/ css | scss
13. elementUI 等 UI 框架需要有自己独立的 css 文件来控制，并且用其组件的顶层命名做区分。src/static/style/ css | scss
14. 公共组件内部避免使用 VUEX 等强耦合逻辑，一切皆以参数来操作。
15. 代码统一使用两空格的缩进。
16. class 命名规范 Node_Modules || Node-Modules
17. 浏览器存储空间所存储的值如果牵扯到某赛事 （品类或者类型）请以 赛事品类*赛事类型*变量名 命名
18. 前端路由页面按， /赛事品类/赛事类型/路有名 的格式来做类型区分 获取后端的字段来动态区分跳转的路由

###### 文件引入规则

1. 公共组件（低耦合组件）建议在其组件内部使用 import 引入 echarts js 等文件，不建议挂载到全局对象下。
