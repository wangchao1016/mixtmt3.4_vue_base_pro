if (window) {
  window.checkCodeSliderOption = {
    template:
      '<div class="form-inline-input">' +
      '<div class="code-box" id="code-box">' +
      '<input type="text" name="code" class="code-input" />' +
      "<i>请按住滑块，拖到最右边</i>" +
      "<p></p>" +
      '<span :class="spanIcon" ></span>' +
      "</div>" +
      "</div>",
    data() {
      return {
        spanIconArr: ["el-icon-d-arrow-right", "el-icon-success"], //[0]:划之前图标； [1]: 划之后图标
        spanIcon: ""
      };
    },
    created() {
      this.spanIcon = this.spanIconArr[0];
    },
    // 调用
    mounted() {
      //   var _this = this;
      //   window.addEventListener("load", function() {
      //     //code是后台传入的验证字符串
      // });
      function moveCode(code, _this) {
        var fn = { codeVluae: code };
        var box = document.querySelector("#code-box"),
          progress = box.querySelector("p"),
          codeInput = box.querySelector(".code-input"),
          evenBox = box.querySelector("span");
        //默认事件
        var boxEven = ["mousedown", "mousemove", "mouseup"];
        //改变手机端与pc事件类型
        if (typeof document.ontouchstart == "object") {
          boxEven = ["touchstart", "touchmove", "touchend"];
        }
        // eslint-disable-next-line no-unused-vars
        var goX, offsetLeft, deviation, evenWidth, endX;
        function moveFn(e) {
          e.preventDefault();
          e = boxEven["0"] == "touchstart" ? e.touches[0] : e || window.event;
          endX = e.clientX - goX;
          endX = endX > 0 ? (endX > evenWidth ? evenWidth : endX) : 0;
          if (endX > evenWidth * 0.9) {
            // 快拉到头时的颜色
            progress.innerText = "松开验证";
            // progress.style.backgroundColor = "#628BFF";
            evenBox.style.backgroundColor = "#fff";
            evenBox.style.color = "#68BF7B";
            _this.spanIcon = _this.spanIconArr[1];
          } else {
            progress.innerText = "";
            // progress.style.backgroundColor = "#628BFF";
          }
          progress.style.width = endX + deviation + "px";
          evenBox.style.left = endX + "px";
        }
        function removeFn() {
          document.removeEventListener(boxEven["2"], removeFn, false);
          document.removeEventListener(boxEven["1"], moveFn, false);
          if (endX > evenWidth * 0.9) {
            progress.innerText = "验证通过";
            progress.style.width = evenWidth + deviation + "px";
            evenBox.style.left = evenWidth + "px";
            codeInput.value = fn.codeVluae;
            evenBox.onmousedown = null;
            _this.isCheckCodeHandle(true);
          } else {
            progress.style.width = "0px";
            evenBox.style.left = "0px";
          }
        }
        //获取元素距离页面边缘的距离
        function getOffset(box, direction) {
          var setDirection = direction == "top" ? "offsetTop" : "offsetLeft";
          var offset = box[setDirection];
          var parentBox = box.offsetParent;
          while (parentBox) {
            offset += parentBox[setDirection];
            parentBox = parentBox.offsetParent;
          }
          parentBox = null;
          return parseInt(offset);
        }
        evenBox.addEventListener(
          boxEven["0"],
          function(e) {
            e = boxEven["0"] == "touchstart" ? e.touches[0] : e || window.event;
            goX = e.clientX;
            offsetLeft = getOffset(box, "left");
            deviation = this.clientWidth;
            evenWidth = box.clientWidth - deviation;
            document.addEventListener(boxEven["1"], moveFn, false);
            document.addEventListener(boxEven["2"], removeFn, false);
          },
          false
        );
        fn.setCode = function(code) {
          if (code) fn.codeVluae = code;
        };
        fn.getCode = function() {
          return fn.codeVluae;
        };
        fn.resetCode = function() {
          evenBox.removeAttribute("style");
          progress.removeAttribute("style");
          codeInput.value = "";
        };
        return fn;
      }
      var code = "jsaidaisd656";
      new moveCode(code, this);
    },
    methods: {
      isCheckCodeHandle(boolean) {
        this.$emit("isCheckCodeFlag", boolean);
      }
    }
  };
}
