/* eslint-disable */
import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store/index.js";
import sliderMenuList from "@baseOption/sliderMenuList"
import { http_getCompetitionCategory } from '@utils/js/api.js';

/**
 * 路由懒加载模块 webpackChunkName: "会此名称加载同名下的所有模块"
 * module之下为webpack懒加载模块下所有页面name 或 /path
 * @module login 登陆模块
 *    /login
 * @module home 首页
 *    /home
 * @module notFound 404页面
 *    /notFound
 */

// 首页子级路由
export const homeChildrenRouter = [
  {
    // name要与菜单列表中的path相同
    // 默认跳转 首页 暂时先跳篮球
    path: "/home",
    redirect: "/basketballIndex"
  },
  {
    // 篮球项目
    path: "/home/basketballIndex",
    name: "basketballIndex",
    meta: {
      requiresAuth: false
    },
    component: () =>
      import(/* webpackChunkName: "basketballIndex" */ "../views/home/childrenView/basketball/index.vue")
  },
  {
    // 篮球球员
    path: "/home/basketballPlayer",
    name: "basketballPlayer",
    meta: {
      requiresAuth: false
    },
    component: () =>
      import(/* webpackChunkName: "basketballPlayer" */ "../views/home/childrenView/basketball/player.vue")

  },
  {
    // 篮球球员详细
    path: "/home/basketballPlayer/:playerId",
    name: "basketballPlayerDetail",
    meta: {
      requiresAuth: false
    },
    component: () =>
      import(/* webpackChunkName: "basketballPlayerDetail" */ "../views/home/childrenView/basketball/playerDetail.vue")

  },
  {
    // 篮球球队
    path: "/home/basketballTeam",
    name: "basketballTeam",
    meta: {
      requiresAuth: false
    },
    component: () =>
      import(/* webpackChunkName: "basketballTeam" */ "../views/home/childrenView/basketball/team.vue")

  },
  {
    // 篮球球队详细
    path: "/home/basketballTeam/:teamId",
    name: "basketballTeamDetail",
    meta: {
      requiresAuth: false
    },
    component: () =>
      import(/* webpackChunkName: "basketballTeamDetail" */ "../views/home/childrenView/basketball/teamDetail.vue")

  },
  {
    // 篮球分析
    path: "/home/basketballAnalysis",
    name: "basketballAnalysis",
    meta: {
      requiresAuth: false
    },
    component: () =>
      import(/* webpackChunkName: "basketballAnalysis" */ "../views/home/childrenView/basketball/analysis.vue")

  },
  // 足球项目
  {
    path: "/home/footballIndex",
    name: "footballIndex",
    meta: {
      requiresAuth: false
    },
    components: {
      default: () =>
        import(
          /* webpackChunkName: "footballIndex" */ "../views/home/childrenView/football/index.vue"
        )
    }
  }
];
const routes = [
  {
    path: "/",
    redirect: "/home/basketballIndex"
  },
  // 首页
  {
    path: "/home",
    name: "home",
    meta: {
      requiresAuth: false
    },
    component: () =>
      import(/* webpackChunkName: "home" */ "../views/home/index.vue"),
    children: homeChildrenRouter
  },
  // 无匹配路由 或预留404
   {
    path: "/404",
    name: "404",
    meta: {
      // 是否需要登陆
      requiresAuth: false
    },
    component: () =>
      import(/* webpackChunkName: "statusPage" */ "../components/statusPage/404.vue")
  },
    // 服务器错误 500
    {
      path: "/500",
      name: "500",
      meta: {
        // 是否需要登陆
        requiresAuth: false
      },
      component: () =>
        import(/* webpackChunkName: "statusPage" */ "../components/statusPage/500.vue")
    },
  // {
  //   path: "*"
  //   // redirect: "/"
  // }
];

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "hash",
  routes
});
// 路由监听函数
// 跳转之前
router.beforeEach((to, from, next, vm) => {
  // 开启loading
  // store.commit("setIsShowLoading", true);
  //如果未匹配到路由
  if (to.matched.length === 0) {
    console.log("router匹配失败404");
    next({
      path: "/404"
    })
    //如果上级也未匹配到路由则跳转登录页面，如果上级能匹配到则转上级路由
    from.name ? next({ name: from.name }) : next("/");
  } else {
    console.log("router匹配成功", to.meta.requiresAuth);
    // 检测页面是否需要登陆
    console.log("to:",to)
    const isHomeView = to.matched.some((val)=>{
      if(val.path === '/home'){
        return true
      }
    })
    if(isHomeView){ //根据用户授权过滤home左侧菜单内容
      store.commit('home/setSliderMenuOption', sliderMenuList)
      store.commit('home/setIsClickSecondMenu', false)
      // 根据路由路径自动选中菜单
      const indexPath  = []
      const pathNameArr = to.matched[to.matched.length-1].path.split('/')

      let firstMenuIndex = -1 //标记当前路由在菜单列表中的一级索引
      let pathNameAct = ''  //最后一层路由name,同时也是二级菜单的route name
      sliderMenuList.forEach((parentMenu, pIndex)=>{
        pathNameArr.forEach((routerName)=> {
          if(routerName.indexOf(parentMenu.name)>-1){
            indexPath.push(parentMenu.index)
            firstMenuIndex = pIndex
            pathNameAct = routerName
          }
        })
      })
      // console.log("firstMenuIndex:", firstMenuIndex)
      if(firstMenuIndex> -1){
        if(sliderMenuList[firstMenuIndex].type === 'submenu'){
          const childrenArr = sliderMenuList[firstMenuIndex].children
          childrenArr.forEach(val=>{
            if(val.path === pathNameAct){
              indexPath.push(val.index)
            }
          })
        }
      }
      console.log("indexPath:", indexPath)
      store.commit('home/setSliderMenuActIndex', indexPath); // 根据路由设置菜单被选中
    }
    next()
    // if (to.meta.requiresAuth) {
    //   const token = localStorage.getItem("MixtmtUserInfo");
    //   if (token) {
    //     // 此请求用于判断用户cookie是否可以使用，登陆状态是否过期，防止卡在loading状态
    //     http_getCompetitionCategory();
    //     next();
    //   } else {
    //     next({
    //       path: "/login"
    //     });
    //   }
    // } else {
    //   next();
    // }
  }
});

// 跳转之后
router.afterEach((to, from, next) => {
  // upDataTimeOut 延时函数内部为用户行为埋点
  const upDataTimeOut = setTimeout(() => {
    // 百度统计采集用户行为埋点
    var _hmt = _hmt || [];
    (function() {
      document.getElementById("baidu_tj") &&
        document.getElementById("baidu_tj").remove();
      var hm = document.createElement("script");
      hm.src = "https://hm.baidu.com/hm.js?a6783dac5f463f17f9e88dc4fb82dcee";
      hm.id = "baidu_tj";
      var s = document.getElementsByTagName("script")[0];
      s.parentNode.insertBefore(hm, s);
      clearTimeout(upDataTimeOut);
    })();
  }, 0);
});
export default router;
