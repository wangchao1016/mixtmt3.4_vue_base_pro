import Vue from "vue";
import Vuex from "vuex";

// 首页相关
import home from "./modules/home/index";
import { http_getGameInfo } from "../utils/js/api";
import skinOption from "../skin/skinOption";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    uaBrowserType: {}, //  存储当前浏览器终端类型
    skinOption, //  存储所有皮肤主题
    skinPorps: {}, // 存储当前皮肤主题
    isMinImage: false, // 是否显示压缩后图片 true 压缩  false 不压缩
    analysisChartRatio: 1.48, // 分析主题图表宽高比例
    isShowLoading: false, // loading全局
    sideChange: false, // 侧导航是否隐藏或者显示
    sideNavList: null, // 侧导航列表
    MenuList: [] // 导航信息
  },
  getters: {},
  mutations: {
    browserUserAgent(state) {
      // 判断浏览器终端设备
      let ua = navigator.userAgent,
        uaBrowserType = "",
        isWindowsPhone = /(?:Windows Phone)/.test(ua),
        isSymbian = /(?:SymbianOS)/.test(ua) || isWindowsPhone,
        isAndroid = /(?:Android)/.test(ua),
        isFireFox = /(?:Firefox)/.test(ua),
        // isChrome = /(?:Chrome|CriOS)/.test(ua),
        isTablet =
          /(?:iPad|PlayBook)/.test(ua) ||
          (isAndroid && !/(?:Mobile)/.test(ua)) ||
          (isFireFox && /(?:Tablet)/.test(ua)),
        isPhone = /(?:iPhone)/.test(ua) && !isTablet,
        isPc = !isPhone && !isAndroid && !isSymbian;
      if (isAndroid || isPhone) {
        // 手机
        uaBrowserType = "ua_phone";
      } else if (isTablet) {
        // 平板
        uaBrowserType = "ua_tablet";
      } else if (isPc) {
        // pc
        uaBrowserType = "ua_pc";
      }
      console.log("设备类型：", uaBrowserType);
      state.uaBrowserType = uaBrowserType;
      // return {
      //   isTablet,
      //   isPhone,
      //   isAndroid,
      //   isPc
      // };
    },
    setSkinPorps(state, skinStr) {
      //更换皮肤
      state.skinPorps = state.skinOption[skinStr.name];
      if (window) {
        const elementLink = document.querySelectorAll("link[skin]");
        if (elementLink[0] && elementLink[0].href) {
          const cssPath = elementLink[0].href.split("elementUI_")[0];
          console.log("cssPath:", cssPath);
          const newCssName = `${skinStr.name}.css`;
          elementLink[0].href = `${cssPath}elementUI_${newCssName}`;
        }
        console.log(elementLink[0].href);
      }
      console.log("str:", state.skinPorps);
    },
    setIsMinImage(state, data) {
      // 设置是否压缩图片
      state.isMinImage = data;
    },
    setIsShowLoading(state, data) {
      // loading全局   开启在路由钩子函数处
      state.isShowLoading = data;
    },
    setSideNavList(state, data) {
      // 侧导航列表
      state.sideNavList = data;
    },
    setSideChange(state) {
      // 左侧导航宽度发生变化
      state.sideChange = !state.sideChange;
    },
    setMenuList(state, data) {
      // 设置导航信息
      state.MenuList = data;
    }
  },
  actions: {
    action_getGameInfo(context, data) {
      // 当前赛事信息
      http_getGameInfo({
        matchId: data.matchId //比赛ID
      })
        .then(res => {
          console.log(res);
          // context.commit("setGameInfo", res.data);
        })
        .catch(err => {
          console.log(err);
        });
    }
  },
  modules: {
    home
  }
});
