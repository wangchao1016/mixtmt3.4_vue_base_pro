import { default as state } from "./state";
import { default as actions } from "./actions";
import { default as mutations } from "./mutations";

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
