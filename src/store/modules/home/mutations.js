const mutations = {
  // 设置左侧被选中菜单
  setSliderMenuActIndex(state, data) {
    console.log("data:", data);
    if (!data.length) return;
    state.sliderMenuActIndex = data;
  },
  setSliderMenuOption(state, data) {
    // 设置左侧菜单列表
    state.sliderMenuOption = data;
  },
  setIsClickSecondMenu(state, data) {
    // 设置是否可以点击二级菜单返回二级菜单首页
    state.isClickSecondMenu = data;
  },
  setSuciionTop(state, data) {
    state.SuciionTop = data;
  }
};

export default mutations;
