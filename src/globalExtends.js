// 全局通用组件
import Loading from "@components/Loading/index.vue";
import {
  HomeHeaderDiv,
  MenuContentCellSpan,
  UserMenuCellLi,
  HomeSliderContentDiv,
  HomeMainViewDiv,
  IntegratedDataPane,
  IntegratedDataPaneCell,
  SeachBtn,
  PrimaryBtn,
  TabBtnDiv,
  AnalysisObjectDiv,
  IconScreenBtn,
  IconStartAnalysisBtn,
  AnalysisObjectItemSpan,
  OtherScreenAreaDiv,
  TextBtnSpan,
  CustomInputBtnSpan,
  CustomInputAreaBtn,
  AddCustomCondition,
  DelCustomCondition,
  PackUpBtn,
  ScreenResultInfor,
  EditPopResetBtn,
  EditPopOKBtn,
  ClosePopIcon
} from "./skin/components/home";

export default {
  components: {
    Loading,
    HomeHeaderDiv,
    MenuContentCellSpan,
    UserMenuCellLi,
    HomeSliderContentDiv,
    HomeMainViewDiv,
    IntegratedDataPane,
    IntegratedDataPaneCell,
    SeachBtn,
    PrimaryBtn,
    TabBtnDiv,
    AnalysisObjectDiv,
    IconScreenBtn,
    IconStartAnalysisBtn,
    AnalysisObjectItemSpan,
    OtherScreenAreaDiv,
    TextBtnSpan,
    CustomInputBtnSpan,
    CustomInputAreaBtn,
    AddCustomCondition,
    DelCustomCondition,
    PackUpBtn,
    ScreenResultInfor,
    EditPopResetBtn,
    EditPopOKBtn,
    ClosePopIcon
  },
  data() {
    return {};
  },
  computed: {
    uaBrowserType() {
      // 判断当前终端类型
      return this.$store.state.uaBrowserType;
    },
    skinPorps() {
      // 设置皮肤
      return this.$store.state.skinPorps;
    }
  }
};
