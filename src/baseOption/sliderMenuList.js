// home 左侧菜单列表
export default [
  {
    authorize: ["basketball"],
    type: "submenu",
    name: "basketball",
    index: "0",
    title: "篮球大数据",
    iconFont: "&#xe66c;",
    children: [
      {
        index: "0-0",
        title: "首页",
        path: "basketballIndex"
      },
      {
        index: "0-1",
        title: "球员",
        path: "basketballPlayer"
      },
      {
        index: "0-2",
        title: "球队",
        path: "basketballTeam"
      },
      {
        index: "0-3",
        title: "分析工具",
        path: "basketballAnalysis"
      }
    ]
  },
  {
    authorize: ["football"],
    type: "item",
    name: "football",
    index: "1",
    title: "足球大数据",
    iconFont: "&#xe677;",
    path: "footballIndex"
  },
  {
    authorize: ["test"],
    type: "submenu",
    name: "test",
    index: "2",
    title: "测试",
    iconFont: "&#xe667;",
    children: [
      {
        index: "2-1",
        title: "首页",
        path: "testIndex"
      },
      {
        index: "2-2",
        title: "球员",
        path: "testPlayer"
      }
    ]
  }
];
