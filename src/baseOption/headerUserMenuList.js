// 顶部点击头像菜单list
export default [
  {
    title: "会员服务",
    icon: "&#xe66a;",
    isAct: true, // 是否 被选中
    isShow: true, //是否 展示
    routerUrl: ""
  },
  {
    title: "账号管理",
    icon: "&#xe671;",
    isAct: false,
    isShow: true,
    routerUrl: ""
  },
  {
    title: "偏好设置",
    icon: "&#xe667;",
    isAct: false,
    isShow: true,
    routerUrl: ""
  },
  {
    title: "建议反馈",
    icon: "&#xe669;",
    isAct: false,
    isShow: true,
    routerUrl: ""
  }
];
