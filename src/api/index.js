import axios from "axios";
import app from "../main";
import { jqParam } from "@utils/js/index";
const isBuildDev = process.env.VUE_APP_API === "development";
// console.log(`isDevAPI: ${JSON.stringify(process.env)}    isBuildDevAPI: ${process.env.VUE_APP_API}`)
console.todo = function(msg) {
  console.log(
    "%c %s %s %s",
    "color: yellow; background-color: #000;",
    "–",
    msg,
    "– "
  );
};
let httpBase = "";
// 测试
if (isBuildDev) {
  console.todo("test环境");
  // httpBase = "/"
  httpBase = "/";
} else {
  console.todo("online环境");
  // httpBase = "https://cn-wfootball-api.mixtmt.com:8093"   //线上外网 域名
  httpBase = "http://47.94.214.231:9032"; //线上外网 域名
}
// axios全局配置
axios.defaults.headers.common["Content-Type"] =
  "application/x-www-form-urlencoded";
//`withCredentials` 表示跨域请求时是否需要使用凭证
axios.defaults.withCredentials = true;
// 创建一个axios实例
var instance = axios.create({
  baseURL: httpBase,
  headers: {
    "content-type": "application/x-www-form-urlencoded"
  }
});

class $axiosString {
  post(url, param) {
    console.log();
    let promise_xhr = new Promise((resolve, reject) => {
      const temp = jqParam(param, true);
      instance.post(url, temp).then(
        response => {
          // 成功
          if (response.data.status === 0) {
            resolve(response.data);
          }
          if (response.data.status === -1) {
            console.log("接口异常，参数有问题");
            reject(response.data);
          }
          if (response.data.status === 9) {
            console.log("用户登录状态已过期，请重新登录");
            app.$router.push("/login");
            sessionStorage.clear();
            localStorage.clear();
          }
          // 服务器错误
          if (response.data.status === 500) {
            console.log("服务器错误");
            reject(response.data);
          }
          // 用户所属站点冻结
          if (response.data.status === 9001) {
            console.log("用户所属站点冻结");
            reject(response.data);
          }
          // 账号被冻结
          if (response.data.status === 9002) {
            console.log("账号被冻结");
            reject(response.data);
          }
        },
        error => {
          reject(error);
        }
      );
    });
    return promise_xhr;
  }
  get(url) {
    let promise_xhr = new Promise((resolve, reject) => {
      instance.get(url).then(
        response => {
          console.log(response);
          resolve(response.data);
        },
        error => {
          // console.log("root:", error.request, error.response, error.message);
          let errObj = {
            statusCode: null,
            apiLastPath: "",
            tipMessage: ""
          };
          if (error.response) {
            errObj.statusCode = error.response.status;
            errObj.apiLastPath = error.response.data.path
              ? error.response.data.path.split("/").pop()
              : "";
          } else if (error.request) {
            errObj.statusCode = error.request.status;
            errObj.apiLastPath = error.request.responseURL
              ? error.request.responseURL.split("/").pop()
              : "";
          } else {
            if (typeof error.message === "string") {
              errObj.statusCode = error.message.split(" ").pop();
            }
          }
          if (errObj.statusCode == 500) {
            errObj.tipMessage = "服务器错误";
          } else if (errObj.statusCode == 400) {
            errObj.tipMessage = "客服端错误";
          } else {
            errObj.tipMessage = "错误待分析";
          }
          reject(errObj);
        }
      );
    });
    return promise_xhr;
  }
}

export default new $axiosString();

//  GET
// export const http_getCompetitionCategory = () => {
//   return $axios.get(`/api/competition/getCompetitionCategory`);
// };
// pst
// export const http_getCompetitionByCategory = params => {
//   return $axios.post(`/api/competition/getCompetitionByCategory`, params);
// };
