// ------- 篮球大数据 -------
import $axios from "./index";
/* 球队 */
/* 首页 */
//最新比赛
export const http_team_latestSchedule = () => {
  return $axios.get(`/basketball/homepage/latestSchedule`);
};
// 热门球队
export const http_team_hotTeam = () => {
  return $axios.get(`/basketball/homepage/hotTeam`);
};
// 球队数据
export const http_team_getTotalTeamData = () => {
  return $axios.get(`/basketball/team/totalTeamData`);
};
// 赛类项和相应赛季项
export const http_team_getCompetitionAndSeasonItem = () => {
  return $axios.get(`/basketball/team/competitionAndSeasonItem`);
};
// 赛段项
export const http_team_getPhaseItem = params => {
  return $axios.get(
    `/basketball/team/phaseItem?competitionId=${params.competitionId}&seasonId=${params.seasonId}`
  );
};
// 球队项
export const http_team_getTeamItem = params => {
  return $axios.get(
    `/basketball/team/teamItem?competitionId=${params.competitionId}&seasonId=${params.seasonId}`
  );
};
// 球队技术统计_场均
export const http_team_getTeamAvgPage = params => {
  return $axios.get(
    `/basketball/team/technicalStatisticsAvgPage?competitionId=${params.competitionId}&seasonId=${params.seasonId}&phaseId=${params.phaseId}&teamId=${params.teamId}&name=${params.name}&page=${params.page}&size=${params.size}`
  );
};
// 球队技术统计_总数
export const http_team_getTeamSumPage = params => {
  return $axios.get(
    `/basketball/team/technicalStatisticsSumPage?competitionId=${params.competitionId}&seasonId=${params.seasonId}&phaseId=${params.phaseId}&teamId=${params.teamId}&name=${params.name}&page=${params.page}&size=${params.size}`
  );
};
/* 球队详情 */
//球队详情信息
export const http_team_teamInfo = params => {
  return $axios.get(
    `/basketball/team/teaminfo/teamInfo?teamId=${params.teamId}&competitionId=${params.competitionId}`
  );
};
//联盟对比
export const http_team_seasonTeamStatisticsUnionContrast = params => {
  return $axios.get(
    `/basketball/team/teaminfo/seasonTeamStatisticsUnionContrast?competitionId=${params.competitionId}&seasonId=${params.seasonId}&teamId=${params.teamId}`
  );
};
//球队数据
export const http_team_seasonTeamStatistics = params => {
  return $axios.get(
    `/basketball/team/teaminfo/seasonTeamStatistics?competitionId=${params.competitionId}&seasonId=${params.seasonId}&teamId=${params.teamId}`
  );
};
//阵容
export const http_team_seasonTeamSquad = params => {
  return $axios.get(
    `/basketball/team/teaminfo/seasonTeamSquad?competitionId=${params.competitionId}&seasonId=${params.seasonId}&teamId=${params.teamId}`
  );
};
// 球队得分效率区域图
export const http_team_seasonTeamShootEfficiency = params => {
  return $axios.get(
    `/basketball/team/teaminfo/seasonTeamShootEfficiency?competitionId=${params.competitionId}&seasonId=${params.seasonId}&teamId=${params.teamId}`
  );
};
// 球队出手比例分布图
export const http_team_seasonTeamFgaPct = params => {
  return $axios.get(
    `/basketball/team/teaminfo/seasonTeamFgaPct?competitionId=${params.competitionId}&seasonId=${params.seasonId}&teamId=${params.teamId}`
  );
};
// 球队得分方式分布
export const http_team_seasonTeamPtsDistribution = params => {
  return $axios.get(
    `/basketball/team/teaminfo/seasonTeamPtsDistribution?competitionId=${params.competitionId}&seasonId=${params.seasonId}&teamId=${params.teamId}`
  );
};
// 制胜四要素
export const http_team_seasonTeamFourElements = params => {
  return $axios.get(
    `/basketball/team/teaminfo/seasonTeamFourElements?competitionId=${params.competitionId}&seasonId=${params.seasonId}&teamId=${params.teamId}`
  );
};
// 创造进攻机会
export const http_team_seasonTeamOffensiveOpportunity = params => {
  return $axios.get(
    `/basketball/team/teaminfo/seasonTeamOffensiveOpportunity?competitionId=${params.competitionId}&seasonId=${params.seasonId}&teamId=${params.teamId}&period=${params.period}`
  );
};
// 球队阵容（各种王）
export const http_team_seasonTeamLineup = params => {
  return $axios.get(
    `/basketball/team/teaminfo/seasonTeamLineup?competitionId=${params.competitionId}&seasonId=${params.seasonId}&teamId=${params.teamId}`
  );
};
// 球队数据排名
export const http_team_seasonTeamDataRank = params => {
  return $axios.get(
    `/basketball/team/teaminfo/seasonTeamDataRank?competitionId=${params.competitionId}&seasonId=${params.seasonId}&teamId=${params.teamId}`
  );
};
// 球队基础数据
export const http_team_seasonTeamBasicsData = params => {
  return $axios.get(
    `/basketball/team/teaminfo/seasonTeamBasicsData?competitionId=${params.competitionId}&seasonId=${params.seasonId}&teamId=${params.teamId}`
  );
};

// 球队投篮热图
export const http_team_seasonTeamShootArea = params => {
  return $axios.get(
    `/basketball/team/teaminfo/seasonTeamShootArea?competitionId=${params.competitionId}&seasonId=${params.seasonId}&teamId=${params.teamId}`
  );
};

// 球队介绍
export const http_team_seasonTeamProfile = params => {
  return $axios.get(
    `/basketball/team/teaminfo/seasonTeamProfile?competitionId=${params.competitionId}&seasonId=${params.seasonId}&teamId=${params.teamId}`
  );
};
