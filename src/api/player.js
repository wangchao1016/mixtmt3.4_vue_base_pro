// ------- 篮球大数据 -------
import $axios from "./index";
/* 球员 */
/* 首页 */
// 热门球员
export const http_player_hotPlayer = () => {
  return $axios.get(`/basketball/homepage/hotPlayer`);
};
// 球员数据
export const http_player_getTotalPlayerData = () => {
  return $axios.get(`/basketball/player/totalPlayerData`);
};
// 赛类项和相应赛季项
export const http_player_getCompetitionAndSeasonItem = () => {
  return $axios.get(`/basketball/player/competitionAndSeasonItem`);
};
// 赛段项
export const http_player_getPhaseItem = params => {
  return $axios.get(
    `/basketball/player/phaseItem?competitionId=${params.competitionId}&seasonId=${params.seasonId}`
  );
};
// 球员技术统计_场均
export const http_player_getPlayerAvgPage = params => {
  return $axios.get(
    `/basketball/player/technicalStatisticsAvgPage?competitionId=${params.competitionId}&seasonId=${params.seasonId}&phaseId=${params.phaseId}&position=${params.position}&name=${params.name}&page=${params.page}&size=${params.size}`
  );
};
// 球员技术统计_总数
export const http_player_getPlayerSumPage = params => {
  return $axios.get(
    `/basketball/player/technicalStatisticsSumPage?competitionId=${params.competitionId}&seasonId=${params.seasonId}&phaseId=${params.phaseId}&position=${params.position}&name=${params.name}&page=${params.page}&size=${params.size}`
  );
};
/*球员详情*/
// 球员详情及联盟对比
export const http_player_playerDetail = params => {
  return $axios.get(
    `/basketball/player/playerDetail?competitionId=${params.competitionId}&seasonId=${params.seasonId}&playerId=${params.playerId}`
  );
};
// 球员数据_总数
export const http_player_playerDataSum = params => {
  return $axios.get(
    `/basketball/player/playerDataSum?competitionId=${params.competitionId}&seasonId=${params.seasonId}&playerId=${params.playerId}`
  );
};
// 球员数据_场均
export const http_player_playerDataAvg = params => {
  return $axios.get(
    `/basketball/player/playerDataAvg?competitionId=${params.competitionId}&seasonId=${params.seasonId}&playerId=${params.playerId}`
  );
};
// 球员得分区域分布贡献
export const http_player_playerAreaContribution = params => {
  return $axios.get(
    `/basketball/player/playerAreaContribution?competitionId=${params.competitionId}&seasonId=${params.seasonId}&playerId=${params.playerId}`
  );
};
// 球员得分效率统计
export const http_player_playerShootingEfficiency = params => {
  return $axios.get(
    `/basketball/player/playerShootingEfficiency?competitionId=${params.competitionId}&seasonId=${params.seasonId}&playerId=${params.playerId}`
  );
};
// 球员基础数据
export const http_player_playerBasicsData = params => {
  return $axios.get(
    `/basketball/player/playerBasicsData?competitionId=${params.competitionId}&seasonId=${params.seasonId}&playerId=${params.playerId}`
  );
};
// 球员高阶数据
export const http_player_playerHighData = params => {
  return $axios.get(
    `/basketball/player/playerHighData?competitionId=${params.competitionId}&seasonId=${params.seasonId}&playerId=${params.playerId}`
  );
};
// 球员比赛贡献
export const http_player_playerGameContribution = params => {
  return $axios.get(
    `/basketball/player/playerGameContribution?competitionId=${params.competitionId}&seasonId=${params.seasonId}&playerId=${params.playerId}`
  );
};
// 球员比赛贡献
export const http_player_playerDataRank = params => {
  return $axios.get(
    `/basketball/player/playerDataRank?competitionId=${params.competitionId}&seasonId=${params.seasonId}&playerId=${params.playerId}`
  );
};

// 球员投篮热区
export const http_player_playerShootArea = params => {
  return $axios.get(
    `/basketball/player/playerShootArea?competitionId=${params.competitionId}&seasonId=${params.seasonId}&playerId=${params.playerId}`
  );
};

// 球员内线分析
export const http_player_playerPaintAnalysis = params => {
  return $axios.get(
    `/basketball/player/playerPaintAnalysis?competitionId=${params.competitionId}&seasonId=${params.seasonId}&playerId=${params.playerId}`
  );
};
