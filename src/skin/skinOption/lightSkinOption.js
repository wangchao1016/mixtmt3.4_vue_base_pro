// 浅色系
export default {
  name: "lightSkin",
  OuterBgColor: {
    default: "#fff",
    type: String
  },
  InnerContentBgColor: {
    default: "#999",
    type: String
  },
  CheckCodeBorderColor: {
    //滑块外层描边
    default: "red",
    type: String
  },
  CheckCodeBottomBgColor: {
    //滑块底层背景色
    default: "#282d52",
    type: String
  },
  CheckCodeUpBgColor: {
    // 滑块顶层背景色
    default: "green",
    type: String
  },
  LoginBtnColor: {
    // 【登录】按钮字颜色
    default: "#fff",
    type: String
  },
  LoginBtnBgColor: {
    // 【登录】按钮背景颜色
    default: "#628BFF",
    type: String
  },
  LoginDefaultFontColor: {
    // 登录注册页默认字体颜色
    default: "#ABADBD",
    type: String
  },
  LoginClickFontColor: {
    // 登录注册页可以点击字体颜色
    default: "red",
    type: String
  }
};
