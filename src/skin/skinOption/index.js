import darkSkin from "./darkSkinOption";
import lightSkin from "./lightSkinOption";
export default {
  darkSkin,
  lightSkin
};
