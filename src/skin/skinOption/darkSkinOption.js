// 深色系
import branch from "./darkSkinOption_branch";
export default Object.assign(branch, {
  name: "darkSkin",
  StatusColorUI1: {
    // UI图规范 顺序 先从左往右 后自上而下 第1
    default: "#2D325A",
    type: String
  },
  StatusColorUI2: {
    // UI图规范 顺序 先从左往右 后自上而下 第2
    default: "#202342",
    type: String
  },
  StatusColorUI3: {
    // UI图规范 顺序 先从左往右 后自上而下 第3
    default: "#353195",
    type: String
  },
  StatusColorUI4: {
    // UI图规范 顺序 先从左往右 后自上而下 第4
    default: "#C12C3A",
    type: String
  },
  StatusColorUI5: {
    // UI图规范 顺序 先从左往右 后自上而下 第5
    default: "#F0A638",
    type: String
  },
  StatusColorUI6: {
    // UI图规范 顺序 先从左往右 后自上而下 第6
    default: "#5960AE",
    type: String
  },
  TextColorBlackUI1: {
    // UI图规范 顺序 黑色字体 第1
    default: "#333",
    type: String
  },
  TextColorBlackUI2: {
    // UI图规范 顺序 黑色字体 第2
    default: "#555",
    type: String
  },
  TextColorBlackUI3: {
    // UI图规范 顺序 黑色字体 第3
    default: "#666",
    type: String
  },
  TextColorBlackUI4: {
    // UI图规范 顺序 黑色字体 第4
    default: "#888",
    type: String
  },
  TextColorBlackUI5: {
    // UI图规范 顺序 黑色字体 第5
    default: "#7C8088",
    type: String
  },
  TextColorBlueUI0: {
    // UI图规范 顺序 蓝色字体 第0
    // home 标题字体颜色
    default: "#2D325A",
    type: String
  },
  TextColorBlueUI1: {
    // UI图规范 顺序 蓝色字体 第1
    default: "#353195",
    type: String
  },
  TextColorBlueUI2: {
    // UI图规范 顺序 蓝色字体 第2
    default: "#5960AE",
    type: String
  },
  TextColorBlueUI3: {
    // UI图规范 顺序 蓝色字体 第3
    default: "#8C93C3",
    type: String
  },
  LineColorUI1: {
    // 顺序 自上而下 先左后右
    // UI图规范 顺序 线 第1
    default: "#F6F8FB",
    type: String
  },
  LineColorUI2: {
    // UI图规范 顺序 线 第2
    default: "#F8F8F8",
    type: String
  },
  LineColorUI3: {
    // UI图规范 顺序 线 第3
    default: "#ECEDF4",
    type: String
  },
  LineColorUI4: {
    // UI图规范 顺序 线 第4
    default: "#E9E9E9",
    type: String
  },
  LineColorUI5: {
    // UI图规范 顺序 线 第5
    default: "#DEDEDE",
    type: String
  },
  LineColorUI6: {
    // UI图规范 顺序 线 第6
    default: "#E8E8E8",
    type: String
  },
  LineColorUI7: {
    // UI图规范 顺序 线 第7
    default: "#7881B6",
    type: String
  },
  LineColorUI8: {
    // UI图规范 顺序 线 第8
    default: "#CED1E7",
    type: String
  },
  LineColorUI9: {
    // UI图规范 顺序 线 第9
    default: "#8C93C3",
    type: String
  },
  BgColorBlueUI1: {
    // background color 蓝色 UI 第1
    default: "#2D325A",
    type: String
  },
  BgColorBlueUI2: {
    // background color 蓝色 UI 第2
    default: "#3E467F",
    type: String
  },
  BgColorBlueUI3: {
    // background color 蓝色 UI 第3
    default: "#5960AE",
    type: String
  },
  BgColorBlueUI4: {
    // background color 蓝色 UI 第4
    default: "#CED1E7",
    type: String
  },
  BgColorWhiteUI0: {
    // background color 白色 UI 第0
    default: "#fff",
    type: String
  },
  BgColorWhiteUI1: {
    // background color 白色 UI 第1
    default: "#F6F8FB",
    type: String
  },
  TextColorWhiteUI0: {
    // 发白的或者浅色的都叫白色
    // background color  白色UI 纯白色
    default: "#fff",
    type: String
  },
  TextColorWhiteUI1: {
    // 发白的或者浅色的都叫白色
    // background color  白色UI 第2
    default: "#F6F8FB",
    type: String
  },
  PopBgColorUI1: {
    // 提示框背景色 UI 第1
    default: "rgba(0, 0, 0, 0.4)",
    type: String
  },
  HomeMainViewBgColor: {
    // home 主视图背景色
    default: "#fff",
    type: String
  },
  ActColumnCellBgColor: {
    // 自定义左右切换表格 列被选中时cell的背景色
    default: "rgba(53, 49, 149, .15)",
    type: String
  },
  echartBgColor: {
    // 图表表格背景色
    default: "rgba(62,70,127,0.7)",
    type: String
  },
  //以下需要在筛选
  HomeHeaderHoverFontColor: {
    // home头部导航hover字体色
    default: "#CED1E7",
    type: String
  }
});
