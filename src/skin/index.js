const defaultSkin = process.env.VUE_APP_SKIN
  ? process.env.VUE_APP_SKIN
  : "darkSkin";
export default defaultSkin;
