// 【home】当中的dom组件
import styled from "vue-styled-components";
import skinOption from "../skinOption";
import defaultSkin from "../index";

const skinProps = skinOption[defaultSkin];
console.log("VUE_APP_SKIN:", skinProps);

// 顶部导航 样式
export const HomeHeaderDiv = styled("div", skinProps)`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1000;
  width: 100%;
  height: 64px;
  background-color: ${props => props.BgColorBlueUI1};
  color: ${props => props.TextColorBlueUI2};
  .logoDiv {
    float: left;
    margin-right: -194px;
    padding: 22px 42px 0 30px;
    width: 194px;
    height: 100%;
    overflow: hidden;
    img {
      display: block;
      width: 100%;
      height: auto;
    }
  }
  .headerMenuDiv {
    padding: 0 20px 0 194px;
    width: 100%;
    height: 100%;
    .headerMenuContent {
      padding: 16px 0;
      width: 100%;
      text-align: right;
      .fontBold {
        font-weight: bold;
      }
    }
  }
  .userMenuMain {
    position: fixed;
    top: 74px;
    right: 40px;
    z-index: 1500;
    width: 200px;
    height: auto;
    box-shadow: 2px 2px 10px 0px rgba(41, 45, 83, 0.1);
    .userMenuMain_header {
      display: block;
      padding-left: 24px;
      width: 100%;
      height: 70px;
      line-height: 70px;
      font-size: 16px;
      text-align: left;
    }
    .userMenuMain_body {
      padding: 10px 0;
      width: 100%;
      height: auto;
    }
  }
`;

// 顶部导航 右侧菜单 内容 样式
export const MenuContentCellSpan = styled("span", skinProps)`
  position: relative;
  display: inline-block;
  padding-left: 30px;
  line-height: 32px;
  vertical-align: middle;
  &.cursorPointer:hover {
    cursor: pointer;
    color: ${props => props.HomeHeaderHoverFontColor};
  }
  .pr12 {
    padding-right: 12px;
  }
  &.phoneIcon {
    color: ${props => props.TextColorWhiteUI0};
    .iconfont {
      vertical-align: middle;
    }
  }
  i {
    display: inline-block;
  }
  .userImg {
    display: inline-block;
    width: 32px;
    height: 32px;
    border-radius: 16px;
    overflow: hidden;
    vertical-align: middle;
    img {
      display: block;
      width: 100%;
    }
  }
`;

// 顶部导航 点击头像时 用户菜单 样式
export const UserMenuCellLi = styled("li", skinProps)`
  width: 100%;
  height: 50px;
  color: ${props => props.TextColorBlackUI5};
  line-height: 50px;
  text-align: left;
  font-size: 16px;
  vertical-align: middle;
  cursor: pointer;
  &:hover,
  &.act {
    background-color: ${props => props.TextColorWhiteUI1};
  }
  &.act {
    color: ${props => props.TextColorBlueUI2};
  }
  .iconSpan {
    display: inline-block;
    margin-top: -3px;
    padding-left: 24px;
    width: 60px;
    font-size: 18px;
    vertical-align: middle;
  }
  .menuTitle {
    display: inline-block;
  }
`;

// 左侧菜单样式
export const HomeSliderContentDiv = styled("div", skinProps)`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 100;
  padding-top: 94px;
  width: 100%;
  height: 100%;
  background-color: ${props => props.BgColorBlueUI2};
  overflow: auto;
  .sliderContent_infor {
    width: 194px;
    .icon {
      padding-right: 14px;
      font-size: 20px;
    }
  }
`;

// home 主视图
export const HomeMainViewDiv = styled("div", skinProps)`
  position: fixed;
  z-index: 150;
  background-color: ${props => props.HomeMainViewBgColor};
  border-radius: 30px 0 0 0;
  overflow: auto;
`;

// player team 头部数据面板
export const IntegratedDataPane = styled("div", skinProps)`
  padding-bottom: 20px;
  width: 100%;
  height: auto;
  .dataPane_title {
    display: block;
    padding-bottom: 14px;
    width: 100%;
    line-height: 26px;
    font-size: 18px;
    font-weight: bold;
    text-align: left;
  }
  .dataPane_content {
    width: 100%;
    height: 114px;
  }
`;

// player team 头部数据面板里面单个cell
export const IntegratedDataPaneCell = styled("span", skinProps)`
  position: relative;
  float: left;
  margin-right: 20px;
  width: 276px;
  height: 114px;
  border-radius: 16px;
  text-align: left;
  background-size: 100% 100%;
  background-repeat: no-repeat;
  background-position: left top;
  &.lastPaneCell {
    margin-right: 0;
    width: 278px;
    .paneCell_num {
      font-size: 32px;
    }
  }
  .paneCell_num {
    position: absolute;
    left: 20px;
    top: 16px;
    font-size: 40px;
    line-height: 56px;
  }
  .paneCell_title {
    position: absolute;
    left: 20px;
    top: 74px;
    font-size: 14px;
    line-height: 20px;
    color: rgba(245, 245, 246, 1);
  }
`;

// 搜索按钮
export const SeachBtn = styled("div", skinProps)`
  display: inline-block;
  margin-left: 30px;
  width: 80px;
  height: 42px;
  border-radius: 21px;
  background-color: ${props => props.BgColorBlueUI3};
  &:hover {
    background-color: ${props => props.BgColorBlueUI2};
  }
  &.ua_pc {
    cursor: pointer;
  }
  i {
    display: block;
    height: 42px;
    text-align: center;
    line-height: 42px;
    font-size: 16px;
    color: ${props => props.TextColorWhiteUI0};
  }
`;

// 主要点击按钮
export const PrimaryBtn = styled("div", skinProps)`
  display: inline-block;
  padding: 0 24px;
  width: auto;
  height: 42px;
  border-radius: 21px;
  line-height: 42px;
  font-size: 16px;
  text-align: center;
  color: ${props => props.TextColorWhiteUI0};
  background-color: ${props => props.BgColorBlueUI3};
  &:hover {
    background-color: ${props => props.BgColorBlueUI2};
  }
  &.ua_pc {
    cursor: pointer;
  }
  i {
    display: block;
    height: 42px;
  }
`;

// 分析工具 顶部 【球员分析】【球队分析】 tab btn
export const TabBtnDiv = styled("div", skinProps)`
  margin: 30px 0;
  width: 100%;
  height: auto;
  vertical-align: bottom;
  text-align: left;
  span {
    display: inline-block;
    margin-right: 20px;
    width: auto;
    line-height: 26px;
    font-size: 16px;
    vertical-align: bottom;
    font-weight: bold;
    color: ${props => props.TextColorBlackUI3};
    cursor: pointer;
    &:last-child {
      margin-left: 0;
    }
  }
  span.act {
    line-height: 34px;
    font-size: 24px;
    color: ${props => props.TextColorBlueUI1};
  }
  span:hover {
    color: ${props => props.TextColorBlueUI1};
  }
`;

// 分析工具中 分析对象 dom
export const AnalysisObjectDiv = styled("div", skinProps)`
  position: relative;
  margin-bottom: 14px;
  width: 100%;
  min-height: 82px;
  height: auto;
  border-radius: 6px;
  background-color: ${props => props.BgColorWhiteUI1};
  .analysisObject_title {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 2;
    width: 104px;
    line-height: 82px;
    font-size: 16px;
    font-weight: bold;
    text-align: center;
  }
  .analysisObject_result {
    width: 100%;
    padding: 20px 194px 8px 104px;
    height: auto;
    text-align: left;
    & > span {
      margin: 0 12px 12px 0;
    }
  }
  .analysisObject_btnArea {
    position: absolute;
    right: 30px;
    top: 20px;
    z-index: 3;
    display: block;
    width: 134px;
    height: 42px;
    font-size: 16px;
    text-align: center;
  }
`;

// 【筛选】 按钮
export const IconScreenBtn = styled("span", skinProps)`
  display: inline-block;
  width: 100%;
  height: auto;
  border-radius: 6px;
  font-weight: bold;
  text-align: center;
  color: ${props => props.TextColorWhiteUI0};
  vertical-align: middle;
  background-color: ${props => props.BgColorBlueUI3};
  &:hover {
    background-color: ${props => props.BgColorBlueUI2};
  }
  &.ua_pc {
    cursor: pointer;
  }
  i {
    display: inline-block;
    vertical-align: middle;
    line-height: 42px;
  }
  & .iconfont {
    padding-right: 10px;
    font-size: 14px;
  }
`;

// 【确定】 按钮
export const EditPopOKBtn = styled("span", skinProps)`
  display: inline-block;
  width: 80px;
  height: auto;
  border-radius: 6px;
  font-weight: bold;
  text-align: center;
  line-height: 42px;
  color: ${props => props.TextColorWhiteUI0};
  vertical-align: middle;
  background-color: ${props => props.BgColorBlueUI3};
  &:hover {
    background-color: ${props => props.BgColorBlueUI2};
  }
  &.ua_pc {
    cursor: pointer;
  }
`;

// 【取消】 按钮
export const EditPopResetBtn = styled("span", skinProps)`
  display: inline-block;
  margin-right: 20px;
  width: 80px;
  height: auto;
  border-radius: 6px;
  font-weight: bold;
  text-align: center;
  border: 1px solid ${props => props.LineColorUI8};
  line-height: 42px;
  color: ${props => props.TextColorBlueUI2};
  vertical-align: middle;
  background-color: ${props => props.BgColorWhiteUI0};
  &:hover {
    color: ${props => props.TextColorBlueUI1};
    background-color: ${props => props.BgColorBlueUI4};
  }
  &.ua_pc {
    cursor: pointer;
  }
`;

// 【开始分析】 按钮
export const IconStartAnalysisBtn = styled("span", skinProps)`
  position: absolute;
  bottom: 30px;
  right: 30px;
  z-index: 3;
  display: inline-block;
  width: 134px;
  height: auto;
  font-size: 16px;
  font-weight: bold;
  border-radius: 6px;
  text-align: center;
  color: ${props => props.TextColorWhiteUI0};
  vertical-align: middle;
  background-color: ${props => props.BgColorBlueUI2};
  &:hover {
    background-color: ${props => props.BgColorBlueUI1};
  }
  &.ua_pc {
    cursor: pointer;
  }
  i {
    display: inline-block;
    vertical-align: middle;
    line-height: 42px;
  }
  & .iconfont {
    padding-right: 10px;
    font-size: 14px;
  }
`;

// 分析对象 中 筛选条件 的圆角矩形
export const AnalysisObjectItemSpan = styled("span", skinProps)`
  display: inline-block;
  padding: 10px 12px;
  width: auto;
  height: 42px;
  line-height: 22px;
  border-radius: 6px;
  font-weight: bold;
  border: 1px solid ${props => props.LineColorUI9};
  background-color: ${props => props.BgColorWhiteUI0};
  & > span {
    display: inline-block;
    width: auto;
    line-height: 22px;
    text-align: left;
    font-size: 16px;
  }
  .itemKey {
    padding-right: 10px;
  }
`;

// 分析工具 中间筛选条件区域 【数据情景】等
export const OtherScreenAreaDiv = styled("div", skinProps)`
  position: relative;
  margin-bottom: 20px;
  padding: 30px;
  width: 100%;
  height: auto;
  border-radius: 6px;
  background-color: ${props => props.BgColorWhiteUI1};
  .otherScreenTitle {
    display: block;
    padding-bottom: 10px;
    font-size: 18px;
    text-align: left;
    line-height: 26px;
    font-weight: bold;
  }
  .otherScreenLine {
    display: block;
    padding-bottom: 16px;
    width: 100%;
    height: 52px;
    text-align: left;
    font-size: 16px;
    line-height: 36px;
    font-weight: bold;
    .otherScreenLine_title {
      float: left;
      margin-right: -94px;
      width: 94px;
    }
    .otherScreenLine_content {
      float: left;
      padding-left: 94px;
      width: 100%;
      .otherScreenSelect_title {
        padding-right: 14px;
      }
      &.mr30 > span {
        margin-right: 30px;
      }
      &.mr46 > span {
        margin-right: 46px;
      }
      &.mr50 > span {
        margin-right: 50px;
      }
      & > span:last-child {
        margin-right: 0;
      }
    }
    .otherScreenLine_addCondition {
      display: block;
      width: 100%;
      & > span {
        display: inline-block;
        vertical-align: middle;
      }
      &.mr20 > .inputSpan {
        margin-right: 0;
        width: 208px;
      }
      &.mr20 > span {
        margin-right: 20px;
      }
      &.mr20 > .inputSpan.addCondition {
        margin-right: 0;
        padding-bottom: 2px;
        width: 86px;
      }
      &.mr20 > .divisionSapn {
        margin-right: 0;
        padding: 0 10px;
      }
      & > span:last-child {
        margin-right: 0;
      }
    }
    &:last-child {
      padding-bottom: 0;
      height: 36px;
    }
  }
  &.last .otherScreenLine:last-child {
    height: auto;
  }
`;

// 分析工具 数据情景 后面可点击的文字
export const TextBtnSpan = styled("span", skinProps)`
  display: inline-block;
  width: auto;
  color: ${props => props.TextColorBlackUI3};
  cursor: pointer;
  &.act,
  &:hover {
    color: ${props => props.TextColorBlueUI1};
  }
`;

// 分析工具 筛选弹层层  点击【自定义】=> 【input】
export const CustomInputBtnSpan = styled("span", skinProps)`
  display: inline-block;
  width: auto;
  color: ${props => props.TextColorBlackUI3};
  cursor: pointer;
  vertical-align: middle;
  span {
    vertical-align: middle;
    display: inline-block;
  }
  .customInputArea {
    .inputSpan {
      padding-bottom: 2px;
      width: 86px;
    }
    .divisionSapn {
      padding: 0 10px;
    }
  }
  &:hover {
    color: ${props => props.TextColorBlueUI1};
  }
`;

// 分析工具 筛选弹层  点击【自定义】=> 【确定】
export const CustomInputAreaBtn = styled("span", skinProps)`
  margin-left: 20px;
  width: 86px;
  line-height: 32px;
  text-align: center;
  border-radius: 6px;
  color: ${props => props.TextColorWhiteUI0};
  background-color: ${props => props.BgColorBlueUI3};
  &:hover {
    background-color: ${props => props.BgColorBlueUI2};
  }
  &.ua_pc {
    cursor: pointer;
  }
`;

// 分析工具 筛选弹层  添加【+】按钮
export const AddCustomCondition = styled("span", skinProps)`
  display: inline-block;
  margin-left: 96px;
  width: 30px;
  line-height: 30px;
  text-align: center;
  vertical-align: middle;
  color: ${props => props.TextColorBlueUI2};
  &.iconfont {
    font-size: 30px;
  }
  &:hover {
    color: ${props => props.TextColorWhiteUI0};
    background-color: ${props => props.BgColorBlueUI2};
  }
  &.ua_pc {
    cursor: pointer;
  }
`;

// 分析工具 筛选弹层  删除【x】按钮
export const DelCustomCondition = styled("span", skinProps)`
  display: inline-block;
  margin-left: 50px;
  width: 30px;
  line-height: 30px;
  text-align: center;
  vertical-align: middle;
  color: ${props => props.TextColorBlackUI3};
  &.iconfont {
    font-size: 18px;
  }
  &:hover {
    color: ${props => props.StatusColorUI4};
  }
  &.ua_pc {
    cursor: pointer;
  }
`;

// 分析工具 筛选弹层  点击【收起选项】|| 【更多选项】
export const PackUpBtn = styled("span", skinProps)`
  width: 120px;
  display: inline-block;
  vertical-align: middle;
  color: ${props => props.TextColorBlueUI2};
  &:hover {
    color: ${props => props.TextColorBlueUI1};
  }
  i {
    line-height: 22px;
    font-size: 16px;
    display: inline-block;
    vertical-align: middle;
  }
  i.packUpFont {
    padding-right: 12px;
  }
  &.ua_pc {
    cursor: pointer;
  }
`;

// 分析结果的内容
export const ScreenResultInfor = styled("span", skinProps)`
  display: block;
  margin-bottom: 14px;
  padding: 18px 30px;
  width: 100%;
  height: auto;
  line-height: 28px;
  font-size: 20px;
  border-radius: 6px;
  background-color: ${props => props.BgColorBlueUI2};
  color: ${props => props.TextColorWhiteUI0};
  & span {
    display: inline-block;
    vertical-align: middle;
  }
  .iconfont {
    font-size: 18px;
    padding-right: 14px;
  }
`;

export const ClosePopIcon = styled("span", skinProps)`
  float: right;
  margin: 20px 0;
  line-height: 34px;
  font-size: 20px;
  color: ${props => props.TextColorBlueUI3};
  &:hover {
    color: ${props => props.TextColorBlueUI1};
  }
  &.ua_pc {
    cursor: pointer;
  }
`;
