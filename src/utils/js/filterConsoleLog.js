// 将console.log 替换成 console.debug
class FilterConsoleLog {
  constructor(options) {
    this.options = options;
    this.externalModules = {};
  }

  apply(compiler) {
    let reg = /console.log\(/g;
    let rep = "console.debug(";
    compiler.hooks.emit.tap("CodeBeautify", compilation => {
      // console.log("compilation:", compilation);
      let fileTypeReg = /\.js$/gi;
      Object.keys(compilation.assets).forEach(data => {
        // console.log(data, fileTypeReg.test(data))
        if (fileTypeReg.test(data)) {
          // console.log("fileTypeReg", data);
          let content = compilation.assets[data].source(); // 要处理的文本
          content = content.replace(reg, rep);
          compilation.assets[data] = {
            source() {
              return content;
            },
            size() {
              return content.length;
            }
          };
        }
      });
    });
  }
}
module.exports = FilterConsoleLog;
