/**
 * 方法目录
 * 添加新方法时同步更新
 * @exports toImage toImage 将canvas导为图片
 * @exports getFacilityInfo 检测当前设备是否为ios
 * @exports storageFun 调用缓存方法
 * @exports formValidation 表单验证
 * @exports empty 去除字符串中的空值
 * @exports valueLength 判断字符串的占位长度
 * @exports isTrue 判断一个变量是否有内容或是否为真，包括数组与对象
 * @exports typeOf 判断一个值的类型
 * @exports jqParam js参数序列化
 * @exports zeroFill 补零
 * @exports isContentHidden 检测文字长度内容是否超出父盒子
 * @exports timeToMinute 将秒转为分钟 + 秒
 */

/**
 * @classdesc toImage 将canvas导为图片
 */
export class toImage {
  constructor() {
    this.imgdata = "";
    this.type = "";
  }
  /**
   *
   * @param {Object} options 接受两个参数 [type] imgdata (必传，传入canvas dom)
   *
   */
  getOptions(options) {
    this.type = options.type || "png";
    this.imgdata = options.el.toDataURL("image/" + this.type);
    this.downLoadImage();
  }
  downLoadImage(imgD, fileN) {
    //cavas 保存图片到本地  js 实现
    var savaFile = function(data, filename) {
      var save_link = document.createElementNS(
        "http://www.w3.org/1999/xhtml",
        "a"
      );
      save_link.href = data;
      save_link.download = filename;
      var event = document.createEvent("MouseEvents");
      event.initMouseEvent(
        "click",
        true,
        false,
        window,
        0,
        0,
        0,
        0,
        0,
        false,
        false,
        false,
        false,
        0,
        null
      );
      save_link.dispatchEvent(event);
    };
    var filename = fileN || "" + new Date().getTime() + "." + this.type;
    // 注意咯 由于图片下载的比较少 就直接用当前几号做的图片名字
    savaFile(imgD || this.imgdata, filename);
  }
}

/**
 * @classdesc formValidation 表单验证
 */
export class formValidation {
  constructor() {}
  /**
   * @function accountNumber 账号判断
   * @param { String } account 需验证字符串
   * @return { Boolean } true 匹配成功， false匹配失败
   */
  accountNumber(account) {
    var reg = /^([a-zA-Z]|[0-9])(\w|-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
    if (!account || account.length <= 0) {
      return false;
    }
    return reg.test(account);
  }
  /**
   * @function accountNumber 密码验证 字母开头  字母数字组合  5-16位字符
   * @param { String } pass 需验证字符串
   * @return { Boolean } true 匹配成功， false匹配失败
   */
  passWord(pass) {
    if (!pass || pass.length < 5 || pass.length > 15) {
      return false;
    }
    var reg1 = new RegExp(/^[a-zA-Z0-9]+$/);
    if (!reg1.test(pass)) {
      return false;
    }
    var reg = /^[a-zA-Z][a-zA-Z0-9]{4,15}$/;
    if (reg.test(pass)) {
      return true;
    } else {
      return false;
    }
  }
}
/**
 * @function empty 去除字符串中的空值
 * @param {String} str  ' 你好 前端 '
 * @return {String} '你好前端'
 */
export function empty(str) {
  var reg = /\s/g;
  return str.replace(reg, "");
}
/**
 * @function valueLength 判断字符串的占位长度
 * @param {String} str
 * @return {Number} 字符长度 中文2 英文字符数字1
 */
export function valueLength(str) {
  var len = 0;
  for (var i = 0; i < str.length; i++) {
    if (str[i].charCodeAt(0) > 299) {
      len += 2;
    } else {
      len += 1;
    }
  }
  return len;
}
/**
 * @function isTrue 判断一个变量是否有内容或是否为真，包括数组与对象
 * @param {*} value 需要判断的值
 */
export function isTrue(value) {
  if (!value) {
    return false;
  }
  if (typeOf(value) === "array") {
    if (value.length > 0) {
      return true;
    } else {
      return false;
    }
  } else if (typeOf(value) === "object") {
    for (var key in value) {
      return true;
    }
    return false;
  } else {
    return true;
  }
}
/**
 * @function typeOf 判断一个值的类型
 * @param {*} value 需要判断的值
 * @return {String} number string null undefined boolean array object symbol
 */
export function typeOf(value) {
  const v = Object.prototype.toString
    .call(value)
    .slice(8, -1)
    .toLowerCase();
  return v;
}

/**
 * @function getFacilityInfo 检测当前设备是否为ios
 */
export const getFacilityInfo = () => {
  var UserClient = navigator.userAgent.toLowerCase();
  var IsIPad = UserClient.match(/ipad/i) == "ipad";
  var IsIphoneOs = UserClient.match(/iphone os/i) == "iphone os";
  var IsMidp = UserClient.match(/midp/i) == "midp";
  var IsUc7 = UserClient.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
  var IsUc = UserClient.match(/ucweb/i) == "ucweb";
  var IsAndroid = UserClient.match(/android/i) == "android";
  var IsCE = UserClient.match(/windows ce/i) == "windows ce";
  var IsWM = UserClient.match(/windows mobile/i) == "windows mobile";
  if (
    IsIPad ||
    IsIphoneOs ||
    IsMidp ||
    IsUc7 ||
    IsUc ||
    IsAndroid ||
    IsCE ||
    IsWM
  ) {
    // 移动端
    return true;
  } else {
    // PC端
    return false;
  }
};
/**
 * @function jqParam 参数序列化
 * @param {*} a 规定要序列化的数组或对象
 * @param {Boolean} [traditional] 可选。布尔值，指定是否使用参数序列化的传统样式。
 */
export const jqParam = (a, traditional) => {
  var prefix,
    s = [],
    add = function(key, valueOrFunction) {
      // If value is a function, invoke it and use its return value
      var value =
        typeOf(valueOrFunction) === "function"
          ? valueOrFunction()
          : valueOrFunction;

      s[s.length] =
        encodeURIComponent(key) +
        "=" +
        encodeURIComponent(value == null ? "" : value);
    };

  // If an array was passed in, assume that it is an array of form elements.
  if (typeOf(a) === "array") {
    // Serialize the form elements
    Array.prototype.forEach.call(a, function(item) {
      add(item.name, item.value);
    });
  } else {
    // If traditional, encode the "old" way (the way 1.3.2 or older
    // did it), otherwise encode params recursively.
    for (prefix in a) {
      buildParams(prefix, a[prefix], traditional, add);
    }
  }

  // Return the resulting serialization
  return s.join("&");
};
/**
 *@function buildParams 使用于jqParams
 */
function buildParams(prefix, obj, traditional, add) {
  var name;
  var rbracket = /\[\]$/;
  if (typeOf(obj) === "array") {
    // Serialize array item.
    Array.prototype.forEach.call(obj, function(v, i) {
      if (traditional || rbracket.test(prefix)) {
        // Treat each array item as a scalar.
        add(prefix, v);
      } else {
        // Item is non-scalar (array or object), encode its numeric index.
        buildParams(
          prefix + "[" + (typeof v === "object" && v != null ? i : "") + "]",
          v,
          traditional,
          add
        );
      }
    });
  } else if (!traditional && typeOf(obj) === "object") {
    // Serialize object item.
    for (name in obj) {
      buildParams(prefix + "[" + name + "]", obj[name], traditional, add);
    }
  } else {
    // Serialize scalar item.
    add(prefix, obj);
  }
}

/**
 * @namespace {Object} storageFun 调用缓存方法
 */

export const storageFun = {
  setSession: function(name, value) {
    //设置session
    var valueStr = "";
    if (typeof value === "object") {
      valueStr = JSON.stringify(value);
    } else {
      valueStr = value;
    }
    sessionStorage.setItem(name, valueStr);
  },
  setLocal: function(name, value) {
    //设置local
    var valueStr = "";
    if (typeof value === "object") {
      valueStr = JSON.stringify(value);
    } else {
      valueStr = value;
    }
    localStorage.setItem(name, valueStr);
  },
  getSession: function(name) {
    // 获取session
    var backData = "";
    try {
      backData = JSON.parse(sessionStorage.getItem(name));
    } catch (err) {
      backData = sessionStorage.getItem(name);
    }
    return backData;
  },

  getLocal: function(name) {
    // 获取local
    var backData = "";
    try {
      backData = JSON.parse(localStorage.getItem(name));
    } catch (err) {
      backData = localStorage.getItem(name);
    }
    return backData;
  },
  clearStorage() {
    // 清空缓存
    sessionStorage.clear();
    localStorage.clear();
  }
};
/**
 * @function zeroFill 补零函数
 * @param {Number | String} value 需要补零的值
//  * @param {Number} size 值的大小
 */
export const zeroFill = value => {
  return value < 10 ? "0" + value : value;
};

/**
 * @function isContentHidden 检测文字长度内容是否超出父盒子 14px
 * @param {Number} parentWidth 父级盒子宽度
 * @param {String} content
 */
export function isContentHidden(parentWidth, content) {
  if (!isTrue(content)) {
    return false;
  }
  if (content.length <= 4) {
    return false;
  }
  var reg = /[\u4E00-\u9FA5\uF900-\uFA2D]/;
  var regNum = /[0-9]/;
  var regCapitalEnglish = /[A-H|J-Z]/;
  var regLowerCaseEnglish = /[a-h|j-z]/;
  let childrenWidth = 0;
  if (!content) {
    return false;
  }
  // console.log(content, content.length)
  for (let i = 0; i < content.length; i++) {
    if (reg.test(content[i])) {
      childrenWidth += 14;
    } else if (
      regNum.test(content[i]) ||
      regLowerCaseEnglish.test(content[i])
    ) {
      childrenWidth += 7.8;
    } else if (regCapitalEnglish.test(content[i])) {
      childrenWidth += 8.55;
    } else if (regLowerCaseEnglish.test(content[i])) {
      childrenWidth += 3.65;
    }
  }
  // console.log(childrenWidth, parentWidth, childrenWidth > parentWidth)
  if (childrenWidth > parentWidth - 20) {
    return true;
  } else {
    return false;
  }
}

/**
 * @function timeToMinute 将秒转为分钟 + 秒
 */
export function timeToMinute(time) {
  if (time === "-") {
    return "-";
  }
  if (typeof time === "number") {
    return (
      Math.abs(Math.floor((time - (time % 60)) / 60)) +
      ":" +
      Math.abs(Math.floor(time % 60))
    );
  } else if (typeof time === "string") {
    return (
      Math.abs(Math.floor((time - (time % 60)) / 60)) +
      ":" +
      Math.abs(Math.floor(time % 60))
    );
  }
}
