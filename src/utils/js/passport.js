import CryptoJS from "../thirdParty/CryptoJS/aes";
import "../thirdParty/CryptoJS/mode-ecb-min";
/**
 * @file 加密解密方法
 */
let passport = {
  /**
   * @function encrypt 加密
   * @param {*} word
   */
  encrypt(word) {
    var srcs = CryptoJS.enc.Utf8.parse(word);
    var key = CryptoJS.enc.Utf8.parse("o7H8uIM2O5qv65l2");
    var encrypted = CryptoJS.AES.encrypt(srcs, key, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    });
    return encrypted.toString();
  },
  decrypt(word) {
    var key = CryptoJS.enc.Utf8.parse("o7H8uIM2O5qv65l2");
    var decrypt = CryptoJS.AES.decrypt(word, key, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    });
    return CryptoJS.enc.Utf8.stringify(decrypt).toString();
  }
};

export default passport;
