import axios from "axios";
import app from "../../main";
import { jqParam } from "./index";
const isBuildDev = process.env.VUE_APP_API === "development";
// console.log(`isDevAPI: ${JSON.stringify(process.env)}    isBuildDevAPI: ${process.env.VUE_APP_API}`)
console.todo = function(msg) {
  console.log(
    "%c %s %s %s",
    "color: yellow; background-color: #000;",
    "–",
    msg,
    "– "
  );
};
let httpBase = "";
// 测试
if (isBuildDev) {
  console.todo("test环境");
  // httpBase = "/"
  httpBase = "http://47.94.214.231:9032";
} else {
  console.todo("online环境");
  // httpBase = "https://cn-wfootball-api.mixtmt.com:8093"   //线上外网 域名
  httpBase = "http://47.94.214.231:9032"; //线上外网 域名
}
// axios全局配置
axios.defaults.headers.common["Content-Type"] =
  "application/x-www-form-urlencoded";
//`withCredentials` 表示跨域请求时是否需要使用凭证
axios.defaults.withCredentials = true;
// 创建一个axios实例
var instance = axios.create({
  baseURL: httpBase,
  headers: {
    "content-type": "application/x-www-form-urlencoded"
  }
});

class $axiosString {
  post(url, param) {
    console.log();
    let promise_xhr = new Promise((resolve, reject) => {
      const temp = jqParam(param, true);
      instance.post(url, temp).then(
        response => {
          // 成功
          if (response.data.status === 0) {
            resolve(response.data);
          }
          if (response.data.status === -1) {
            console.log("接口异常，参数有问题");
            reject(response.data);
          }
          if (response.data.status === 9) {
            console.log("用户登录状态已过期，请重新登录");
            app.$router.push("/login");
            sessionStorage.clear();
            localStorage.clear();
          }
          // 服务器错误
          if (response.data.status === 500) {
            console.log("服务器错误");
            reject(response.data);
          }
          // 用户所属站点冻结
          if (response.data.status === 9001) {
            console.log("用户所属站点冻结");
            reject(response.data);
          }
          // 账号被冻结
          if (response.data.status === 9002) {
            console.log("账号被冻结");
            reject(response.data);
          }
        },
        error => {
          reject(error);
        }
      );
    });
    return promise_xhr;
  }
  get(url) {
    let promise_xhr = new Promise((resolve, reject) => {
      instance.get(url).then(
        response => {
          // 成功
          if (response.data.status === 0) {
            resolve(response.data);
          }
          if (response.data.status === -1) {
            console.log("接口异常，参数有问题");
            reject(response.data);
          }
          if (response.data.status === 9) {
            console.log("用户登录状态已过期，请重新登录");
            app.$router.push("/login");
            sessionStorage.clear();
            localStorage.clear();
          }
          // 服务器错误
          if (response.data.status === 500) {
            console.log("服务器错误");
            app.$router.push("/500");
            reject(response.data);
          }
          // 用户所属站点冻结
          if (response.data.status === 9001) {
            console.log("用户所属站点冻结");
            reject(response.data);
          }
          // 账号被冻结
          if (response.data.status === 9002) {
            console.log("账号被冻结");
            reject(response.data);
          }
        },
        error => {
          reject(error);
        }
      );
    });
    return promise_xhr;
  }
}

export let $axios = new $axiosString();

// test@newautogroup.com
// fanshi13

// 登录
export const http_userLogin = params => {
  return $axios.post(`/api/user/login`, params);
};
// 退出登录
export const http_userLogout = () => {
  return $axios.get(`/api/user/logout`);
};

/* 首页 */
// 判断进入的是那个赛事品类通知后台
export const http_matchType = url => {
  return $axios.post(`/api${url}`);
};
// 查询用户操作信息，用于异步队列控制
export const http_getDataItemCount = () => {
  return $axios.post(`/api/admin/firstPage/getDataItemCount`);
};
// 收集用户点击的赛事
export const http_insertCompetitionCount = params => {
  return $axios.post(`/api/competition/insertCompetitionCount`, params);
};
// 获取群组列表信息
export const http_getGroupData = () => {
  return $axios.post(`/api/competition/getGroupData`);
};
// 根据比赛ID获取群组列表，用于本赛事内群组切换
export const http_getGroupByCompetitionId = params => {
  return $axios.post(`/api/competition/getGroupByCompetitionId`, params);
};
// 获取赛事品类 GET
export const http_getCompetitionCategory = () => {
  return $axios.get(`/api/competition/getCompetitionCategory`);
};
// 获取赛事列表
export const http_getCompetitionByCategory = params => {
  return $axios.post(`/api/competition/getCompetitionByCategory`, params);
};
// 获取工作台权限数据
export const http_getFixedMenuLocked = () => {
  return $axios.post(`/api/menu/getFixedMenuLocked`);
};
// 获取侧导航功能列表
export const http_getUserPermissionList = () => {
  return $axios.post(`/api/menu/getUserPermissionList`);
};

/* 页面顶部功能栏 */
// 查询报告列表
export const http_getReportList = params => {
  return $axios.post(`/api/competitionReport/getReportList`, params);
};
// 重置自身账号密码
export const http_updateAccountPassword = params => {
  return $axios.post(`/api/client/user/updateAccountPassword`, params);
};

/* 赛程页 */

// 获取比赛组别信息等等
export const http_getSeasonDatas = params => {
  return $axios.post(`/api/competition/getSeasonDatas`, params);
};
// 获取参赛球队
export const http_getTeamListByRoundAndGroup = params => {
  return $axios.post(`/api/competition/getTeamListByRoundAndGroup`, params);
};
// 获取赛程
export const http_getCompetitionSchedules = params => {
  return $axios.post(`/api/competition/getCompetitionSchedules`, params);
};

/* 播出单投送 */
// 查询播单
export const http_getBroadcastItems = params => {
  return $axios.post("api/workbench/getBroadcastItems", params);
};
// 播出单nav包装列表
export const http_getPackageInfos = params => {
  return $axios.post("api/package/getPackageInfos", params);
};
// 查询包装下播出单条目列表
export const http_getPackageItemInfos = params => {
  return $axios.post("api/package/getPackageItemInfos", params);
};
//删除公共播单-单个
export const http_isAddBroadcastItem = params => {
  return $axios.post("api/workbench/isAddBroadcastItem", params);
};
// 清空公共播出单
export const http_clearPublicBroadcastItem = params => {
  return $axios.post("api/workbench/clearPublicBroadcastItem", params);
};
// 删除某包装播出单条目
export const http_delSinglePackageItem = params => {
  return $axios.post("api/package/delSinglePackageItem", params);
};
// 删除某包装播出单条目(批量)
export const http_deletePackageItem = params => {
  return $axios.post("api/package/deletePackageItem", params);
};
// 导入包装播出单全部条目
export const http_addAllPackageItems = params => {
  return $axios.post("api/package/addAllPackageItems", params);
};
// 导入包装播出单单个条目
export const http_addPackageItemInfos = params => {
  return $axios.post("api/package/addPackageItemInfos", params);
};
// 导入包装播出单单个条目 - 弹窗
export const http_getPublicBroadcastItems = params => {
  return $axios.post("/api/workbench/getPublicBroadcastItems", params);
};
// 包装播出单-发送播单
export const http_sendedPackageInfoItems = params => {
  return $axios.post("api/package/sendedPackageInfoItems", params);
};
// 包装播出单-交换播单
export const http_changePackageItems = params => {
  return $axios.post("api/package/changePackageItems", params);
};
// 包装播出单-清空全部
export const http_clearAllPackageItems = params => {
  return $axios.post("api/package/clearAllPackageItems", params);
};
// 锁定包装播出单
export const http_lockedPackageLockInfo = params => {
  return $axios.post("api/package/lockedPackageLockInfo", params);
};
// 解锁包装播出单
export const http_unLockedPackageLockInfo = params => {
  return $axios.post("api/package/unLockedPackageLockInfo", params);
};
// 预览图片
export const http_changeBroadcastItems = params => {
  return $axios.post("api/workbench/changeBroadcastItems", params);
};
// 导航赛事信息
export const http_getGameInfo = params => {
  return $axios.post("api/competition/getGameInfo", params);
};
// 解锁包装播出单
export const http_getCurrentUserDomain = params => {
  return $axios.post("api/user/getCurrentUserDomain", params);
};

/*播出工作台 数据分发 */
// 查询数据项(数据分发和播单编排)
export const http_findDataItem = params => {
  return $axios.post(`/api/workbench/findDataItem`, params);
};
// 根据多个分析项获取数据
export const http_moreDataByAnalysisID = params => {
  return $axios.post(`/api/selectData/getMoreDataByAnalysisID`, params);
};
// 数据项发布/撤回
export const http_isDisplayDataItem = params => {
  return $axios.post(`/api/workbench/isDisplayDataItem`, params);
};
// 刷新数据项
export const http_refreshDataItem = params => {
  return $axios.post(`/api/workbench/refreshDataItem`, params);
};

/* 播出工作台 博导你编排 */
// 一键加入播出单
export const http_aKeyToJoinPublicBroadcastItem = params => {
  return $axios.post(`/api/workbench/aKeyToJoinPublicBroadcastItem`, params);
};
// 预览图片
export const http_genImg = params => {
  return $axios.post(`/api/workbench/genImg`, params);
};
// 预览视频
export const http_genDv = params => {
  return $axios.post(`/api/workbench/genDv`, params);
};
// 获取文件夹下信息 3.2--3.2.2版本独有
export const http_findDataItemForDir = params => {
  return $axios.post(`/api/workbench/findDataItemForDir`, params);
};
// 根据数据项id查询数据项
export const http_findDataItemById = params => {
  return $axios.post(`/api/workbench/findDataItemById`, params);
};
// // 删除数据项/分析项
export const http_deleteItem = params => {
  return $axios.post(`/api/workbench/deleteItem`, params);
};
// 删除文件夹
export const http_deleteDataItemDir = params => {
  return $axios.post(`/api/workbench/deleteDataItemDir`, params);
};
// 编辑数据项
export const http_editDataItem = params => {
  return $axios.post(`/api/workbench/editDataItem`, params);
};
// 文件夹发布撤回
export const http_isDisplayDataItemDir = params => {
  return $axios.post(`/api/workbench/isDisplayDataItemDir`, params);
};

//切数据库 开发时需要
export const http_changeFootBall = () => {
  return $axios.get(`/api/changeDb/changeFootBall`);
};

/*  编辑工作台 */
// 关键数据分析下拉数据
export const http_getAdvancedDataDropDown = () => {
  return $axios.get(`/api/args/getAdvancedDataDropDown`);
};
// 获取阵容分析下拉框
export const http_getLineupAnalysisDropDown = params => {
  return $axios.get(`/api/args/getLineupAnalysisDropDown`, params);
};
export const http_findTeamIdsByGameId = params => {
  return $axios.post(`/api/args/findTeamIdsByGameId`, params);
};
// 球队概览
export const http_getTeamOverview = params => {
  return $axios.post(`/api/competitionCenter/getTeamOverview`, params);
};
// 查询分组及指标 赛程
export const http_getSchedule = params => {
  return $axios.post(`/api/competitionCenter/getSchedule`, params);
};
// 获取阵容分析下拉框
export const http_getTeamLineup = params => {
  return $axios.post(`/api/competitionCenter/getTeamLineup`, params);
};
// //交锋记录
export const http_getConfrontationRecord = params => {
  return $axios.post(`/api/competitionCenter/getConfrontationRecord`, params);
};
//历史比赛记录
export const http_getHistoryCompetitionRecord = params => {
  return $axios.post(
    `/api/competitionCenter/getHistoryCompetitionRecord`,
    params
  );
};
//领先者
export const http_getLeaderPlayer = params => {
  return $axios.post(`/api/competitionCenter/getLeaderPlayer`, params);
};
//高阶数据
export const http_getAdvancedData = params => {
  return $axios.post(`/api/competitionCenter/getAdvancedData`, params);
};
//获取球队数据
export const http_getTeamDataByGameID = params => {
  return $axios.post(`/api/searchData/getTeamDataByGameID`, params);
};
// 制胜四要素
export const http_getFourElementWinning = params => {
  return $axios.post(`/api/competitionCenter/getFourElementWinning`, params);
};
//创造进攻机会
export const http_createAttackChance = params => {
  return $axios.post(`/api/competitionCenter/createAttackChance`, params);
};
//球队对比
export const http_getTeamCompareData = params => {
  return $axios.post(`/api/searchData/getTeamCompareData`, params);
};
//阵容分析接口
export const http_getLineupAnalysis = params => {
  return $axios.post(`/api/competitionCenter/getLineupAnalysis`, params);
};
//积分榜
export const http_getRankData = params => {
  return $axios.post(`/api/searchData/getRankData`, params);
};
//获取多选列表 报告
export const http_getModuleReportList = params => {
  return $axios.post(`/api/competitionReport/getModuleReportList`, params);
};
//上传选项 报告
export const http_saveSelectedModule = params => {
  return $axios.post(`/api/competitionReport/saveSelectedModule`, params);
};

/* 数据洞察 */
// 获取xy下拉框
export const http_getXAxisDataItem = params => {
  return $axios.post(`/api/analysisTool/getXAxisDataItem`, params);
};
export const http_getYAxisDataItem = params => {
  return $axios.post(`/api/analysisTool/getYAxisDataItem`, params);
};
// 获取分析工具数据
export const http_analyzeSearchData = params => {
  return $axios.post(`/api/searchData/analyzeSearchData`, params);
};
// 获取球队列表
export const http_getTeamList = params => {
  return $axios.post(`/api/competition/getTeamList`, params);
};
// 获取数据洞察数据
export const http_searchData = params => {
  return $axios.post(`/api/searchData/searchData`, params);
};
// 查询数据项
export const http_getDataItem = params => {
  return $axios.post(`/api/queryTool/getDataItem`, params);
};
// 保存用户所选数据项
export const http_saveUserSelectedDataItem = params => {
  return $axios.post(`/api/queryTool/saveUserSelectedDataItem`, params);
};

/* 站点管理 */
// webmaster index
// 获取站点导航列表
export const http_queryStatitionMenuList = () => {
  return $axios.post(`/api/menu/queryStatitionMenuList`);
};

// 赛事信息
// GET 赛事状态下拉框
export const http_getCompetitionStatusDropDown = () => {
  return $axios.get(`/api/client/competitionInfo/getCompetitionStatusDropDown`);
};
// 获取赛事信息
export const http_getUserCompetitionInfo = params => {
  return $axios.post(
    `/api/client/competitionInfo/getUserCompetitionInfo`,
    params
  );
};

// 群组管理
// 获取所有赛事
export const http_getStationCompetition = () => {
  return $axios.post(`/api/client/group/getStationCompetition`);
};
// 获取用户群组信息
export const http_getStationGroupList = params => {
  return $axios.post(`/api/client/group/getStationGroupList`, params);
};
// 查询群组信息
export const http_getStationGroupInfo = params => {
  return $axios.post(`/api/client/group/getStationGroupInfo`, params);
};
// 创建群组
export const http_createStationGroup = params => {
  return $axios.post(`/api/client/group/createStationGroup`, params);
};
// 修改群组
export const http_changeStationGroupInfo = params => {
  return $axios.post(`/api/client/group/changeStationGroupInfo`, params);
};
// 删除群组
export const http_removeStationGroup = params => {
  return $axios.post(`/api/client/group/removeStationGroup`, params);
};

// 用户管理
// 获取站点权限、群组列表
export const http_getStationPermissionGroupList = () => {
  return $axios.post(`/api/client/user/getStationPermissionGroupList`);
};
// 查询用户信息
export const http_getStationUserInfo = params => {
  return $axios.post(`/api/client/user/getStationUserInfo`, params);
};
// 创建用户
export const http_createStationUser = params => {
  return $axios.post(`/api/client/user/createStationUser`, params);
};
// 修改用户信息
export const http_changeUserInfo = params => {
  return $axios.post(`/api/client/user/changeUserInfo`, params);
};
// 获取用户数据列表
export const http_getStationUserList = params => {
  return $axios.post(`/api/client/user/getStationUserList`, params);
};
// 激活冻结用户
export const http_activeFreezeStationUser = params => {
  return $axios.post(`/api/client/user/activeFreezeStationUser`, params);
};
// 重置密码
export const http_aresetAccountPassword = params => {
  return $axios.post(`/api/admin/accountManage/aresetAccountPassword`, params);
};
// 删除用户
export const http_removeStationUser = params => {
  return $axios.post(`/api/client/user/removeStationUser`, params);
};

// 日志信息
// 赛事名称下拉框 GET
export const http_getStationCompetitionDropDown = () => {
  return $axios.get(`/api/client/logInfo/getStationCompetitionDropDown`);
};
// 群组下拉框 GET
export const http_getStationGroupDropDown = () => {
  return $axios.get("/api/client/logInfo/getStationGroupDropDown");
};
// 操作类型下拉框 GET
export const http_getOperateTypeDropDown = () => {
  return $axios.get("/api/client/logInfo/getOperateTypeDropDown");
};
// 获取日志列表
export const http_getStationLogInfo = params => {
  return $axios.post("/api/client/logInfo/getStationLogInfo", params);
};

// 主题创建
// 主题详情
export const http_getTemplateInfoByGroupId = params => {
  return $axios.post("/api/workbench/getTemplateInfoByGroupId", params);
};
// 创建下拉
export const http_getTemplateAndGameStage = params => {
  return $axios.post("/api/workbench/getTemplateAndGameStage", params);
};
// 获取所有模
export const http_getClassifiedTemplatesByCompetitionId = params => {
  return $axios.post(
    "/api/workbench/getClassifiedTemplatesByCompetitionId",
    params
  );
};
// 获取数据项
export const http_findTemplateById = params => {
  return $axios.post("/api/workbench/generator/findTemplateById", params);
};
/* 分析项创建  (迁移接口,如有相同接口归于上方注释模块下)*/
// 获取所有模板
export const http_findTemplates = params => {
  return $axios.post(`/api/workbench/generator/findTemplates`, params);
};
// 获取筛选信息
export const http_findTemplateGroup = params => {
  return $axios.post(`/api/workbench/generator/findTemplateGroup`, params);
};
// 创建分析项
export const http_addAnalysisItem = params => {
  return $axios.post(`/api/workbench/generator/addAnalysisItem`, params);
};
// 展开弹窗预览效果
export const http_previewEffect = params => {
  return $axios.post(`/api/workbench/generator/previewEffect`, params);
};
// 展开弹窗预览效果
export const http_getDataByAnalysisID = params => {
  return $axios.post(`/api/selectData/getDataByAnalysisID`, params);
};

// pdf截取页面
export const http_findReportData = params => {
  return $axios.post(`/api/report/findReportData`, params);
};
