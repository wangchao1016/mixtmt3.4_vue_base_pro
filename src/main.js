import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// 退出页地址
const loginOutUrl = process.env.VUE_APP_LOGINOUT
  ? process.env.VUE_APP_LOGINOUT
  : "";

/**
 * 第三方工具
 */
// elemet-ui
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
// 把页面转换为图片
// import html2canvas from "html2canvas";
// import BScroll from "better-scroll";

/**
 * icon
 */
import "./static/iconfont/iconfont.css";
import "./static/iconfont/iconfont.js";
/**
 * libFlexible 适配+方法（px2rem&&rem2px)
 */
import libFlexible from "./utils/js/lib-flexible";

/**
 * @module passport 加解密方法
 */
import passPort from "./utils/js/passport";
/**
 * @modules index 绝大部分挂载在prototype下的方法都在此文件内
 *
 * @classdesc root_formValidation 表单验证
 * @classdesc toImage 将canvas导为图片
 *
 * @function storageFun 浏览器缓存
 * @function empty 去除空字符
 * @function valueLength 判断字符串的占位长度
 * @function isTrue 判断一个变量是否有内容或是否为真，包括数组与对象
 * @function typeOf 判断一个值的类型
 */

import {
  storageFun,
  formValidation,
  empty,
  toImage,
  valueLength,
  isTrue,
  typeOf,
  zeroFill,
  isContentHidden,
  timeToMinute
} from "./utils/js/index";

Vue.prototype.root_passport = passPort;
Vue.prototype.root_storage = storageFun;
Vue.prototype.root_formValidation = new formValidation();
Vue.prototype.root_empty = empty;
Vue.prototype.root_toImage = new toImage();
Vue.prototype.root_valueLength = valueLength;
Vue.prototype.root_isTrue = isTrue;
Vue.prototype.root_typeOf = typeOf;
// Vue.prototype.root_BScroll = BScroll;
Vue.prototype.root_zeroFill = zeroFill;
Vue.prototype.root_store = store;
Vue.prototype.root_isContentHidden = isContentHidden;
Vue.prototype.root_timeToMinute = timeToMinute;
Vue.prototype.root_libFlexible = libFlexible;
Vue.prototype.root_loginOutUrl = loginOutUrl;

Vue.use(ElementUI);

Vue.filter("zerofill", function(value) {
  // 补零方法
  if (!value) return "";
  return value > 9 ? value : "0" + value;
});

export default new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
