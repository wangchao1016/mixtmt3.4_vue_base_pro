import sys
import io

from pathlib import Path


class DirectionTree(object):
    def __init__(self, pathname='.', filename='tree.md'):
        super(DirectionTree, self).__init__()
        self.pathname = Path(pathname)
        self.filename = filename
        self.tree = ''

    def set_path(self, pathname):
        self.pathname = Path(pathname)

    def set_filename(self, filename):
        self.filename = filename

    def generate_tree(self, n=0):
        if self.pathname.is_file():
            self.tree += '  |' * n + '-' * 4 + self.pathname.name + '\n'
        elif self.pathname.is_dir() and self.pathname.name != "node_modules" and self.pathname.name != ".git" and self.pathname.name != "dist":
            self.tree += '  |' * n + '-' * 4 + \
                str(self.pathname.relative_to(self.pathname.parent)) + '\\' + '\n'

            for cp in self.pathname.iterdir():
                self.pathname = Path(cp)
                self.generate_tree(n + 1)

    def save_file(self):
        with io.open(self.filename, 'w', encoding='utf-8') as f:
            f.write(str(self.tree))


if __name__ == '__main__':
    dirtree = DirectionTree()
    if len(sys.argv) == 1:
        dirtree.set_path(Path.cwd())
        dirtree.generate_tree()
        dirtree.save_file()
        print(dirtree.tree)
    elif len(sys.argv) == 2 and Path(sys.argv[1]).exists():
        dirtree.set_path(sys.argv[1])
        dirtree.generate_tree()
        print(dirtree.tree)
    elif len(sys.argv) == 3 and Path(sys.argv[1]).exists():
        dirtree.set_path(sys.argv[1])
        dirtree.generate_tree()
        dirtree.set_filename(sys.argv[2])
        dirtree.save_file()
    else:
        print('???')
